The file experiments contains the calculations used in the article (only our own methods)

Sergio Domı́nguez-Gimeno, Carlos Medrano-Sánchez, Raúl Igual-Catalán, Javier Martı́nez-Cesteros,
Inmaculada Plaza-Garcı́a, An Optimization Approach to Eliminate Crosstalk in Zero Potential Circuits for Reading Resistive Sensor Arrays, IEEE Sensors Journal, accepted on May, 2023 

They can be found by running in a console (I used linux):

$ python experiments.py

See the code experiments.py and the comments there.


