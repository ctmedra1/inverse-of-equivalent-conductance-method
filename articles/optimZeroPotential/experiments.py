#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2023 Sergio Dominguez-Gimeno (sdominguezg@unizar.es),
Carlos Medrano-Sanchez (ctmedra@unizar.es, ctmedra@gmail.com) and
Javier Martinez-Cesteros (javi.mz.cs@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version !!VERSIÓN DE LA LICENCIA of the 
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

The code is associated to the article:

Sergio Domı́nguez-Gimeno, Carlos Medrano-Sánchez, Raúl Igual-Catalán, Javier Martı́nez-Cesteros,
Inmaculada Plaza-Garcı́a, An Optimization Approach to Eliminate Crosstalk in Zero Potential Circuits for Reading Resistive Sensor Arrays, IEEE Sensors Journal, accepted on May 2023


Further improvements have been made (and may published, see README file).


This program finds the resistances of a resistive sensor array in a ZPM
circuit (and the switch resistances too) using both the ideal ZPM circuit
and the method proposed in the article. It is configured as a set 
of simulations under different conditions that are first stored in a 
dictionary and the saved to a csv file  


"""

import numpy as np
import matplotlib.pyplot as plt
import time
import os
# from IPython.display import display

import pandas as pd

import sys

sys.path.append('../../python/')

import zpm_lsqr as zpm


total_time = 0
np.random.seed(2022)
plt.close('all')
# %% GLOBAL VARIABLES
RMAX = zpm.RMAX #maximum resistance value for the mat
CMIN = 1/RMAX
RMIN = zpm.RMIN #minimum resistance value for the mat
CMAX = 1/RMIN

print("The simulation is run for a reduced set of parameter variation")
print("If not the processing time is very long")
print("See the code and change the parameters accordingly to get the article results")
import time
time.sleep(3)


# Iteration parameters
ITERATIONS = 2 #ITERATIONS = 10
SNR = [np.inf] #SNR = [np.inf,130,120,110,100] #si pones [np.inf,100,60], ocurre el bug
CASES = ['Normal'] #CASES = ['Normal','Worst']
Rw = 100 #for the worst case
Rwij = 10000 #for the worst case
RS = [1] #RS = [1,5,10,15,20] #Switches' resistances
TOLERANCES = [0.1] #TOLERANCES = [0.1,1,2,5,10] #Tolerance of resistances
RS_KNOWN = ['No','Yes']
# RS_KNOWN = ['Yes']
SIZES = [4] #SIZES = [4,8,16] #RSA sizes
METHODS = np.array(['lsqr']) #Scalable in future
CALIBCOLS = [330]

fmt = "{:.8g}"

#Estimation of % executed
total_length = ITERATIONS*len(SIZES)*len(CASES)*len(RS)*len(METHODS)*len(SNR)*len(CALIBCOLS)*len(TOLERANCES)
counter = 0
startTime = time.time()
# %% Results managing in a dictionary
# Dictionary to save results:  for each level of noise, for rij random and worst case,
# for each value of rs, for all mats' sizes and for Rs known and unknown

dic_results = {'Iteration':[],
               'Case': [],
               'Rmin':[],
               'Rmax':[],
               'Rw':[],
               'Rwij':[],
               'Rsmax':[],
               'Rsmin':[],
               'Rs Tol': [],
               'CC': [],
               'Size': [],
               'SNR':[],

               'Time (s)': [],
               'Time Rs (s)':[],
               
               'MRE cm': [],
               'MRE cm LSQR': [],
               'MRE cs LSQR': [],
               'MRE worst': [],
               'MRE worst LSQR': [],
    }

# %% For each iteration
for ss in range(ITERATIONS):
    # np.random.seed(2022)
    # %% With noise or not
    for ii in range(len(SNR)):
        if SNR[ii] == np.inf: #Noiseless
            snr = None
        else:
            snr = SNR[ii]
            
            
        # %% For random or worst case
        for jj in range(len(CASES)):      
            # %% Max and Min resistance and tolerances
            for kk in range(len(RS)):
                for zz in range(len(TOLERANCES)):
                    Rsmax = RS[kk]*(1+TOLERANCES[zz]/200)
                    Rsmin = RS[kk]*(1-TOLERANCES[zz]/200)
                    # Rsmax = Rsmin
                    csmax = 1/Rsmin
                    csmin = 1/Rsmax
                        
                    # %% Iterations for all sizes
                    for mm in range(len(SIZES)):
                        for nn in range(len(METHODS)):
                            for bb in range(len(CALIBCOLS)):
                                N = SIZES[mm]
                                M = SIZES[mm]
                                
                                print('Iteration is', ss)
                                print('Size = ',N,'x',M)
                                print('Rs = '+ str(RS[kk]) +' ohms')
                                print('Tolerance = '+ str(TOLERANCES[zz]) +' %')
                                print('Rsmax = '+str(Rsmax)+', Rsmin = '+str(Rsmin))
                                print('SNR = '+ str(snr) +' dB')
                                # print('Is Rs known?', RS_KNOWN[ll])
                                print('Method is', METHODS[nn])
                                print('Calibration Columns =', CALIBCOLS[bb])
                                
                                # Random case: uniformly distributed random function
                                if CASES[jj] == 'Normal':
                                    #Standard case
                                    cm = CMIN + (CMAX-CMIN)*np.random.rand(N,M)
                                    print('Case: Random Rij')
                                    
                                    
                                # Worst case: one resistance is 10K and the rest are 100 ohms
                                elif CASES[jj] == 'Worst':
                                    cm = 1/Rw*np.ones((N,M))
                                    # print(cm)
                                    cm[int(N/2)][int(M/2)] = 1/Rwij #one resistance is 10K approximately in the middle
                                    print('Case: WORST with Rw = ', Rw,' and Rij = ', Rwij)

                                
                                #In all cases, I need cc, as I have to obtain cs from lsqr
                                Mcc = int(np.ceil((N+M)/(N-1)))
                                cs = csmin + (csmax-csmin)*np.random.rand(N+M+Mcc,1)
                                cc = 1/CALIBCOLS[bb]*np.ones((N,Mcc))
                                Ncc,Mcc = cc.shape

                                if METHODS[nn] == 'lsqr':
                                    print('Method is lsqr')
                                    results = zpm.solveExperiment(N, M, cm, cs, cc, 
                                                                  Rsmin, Rsmax, 
                                                                  mth = 'lsqr', 
                                                                  Rw = Rw, Rwij = Rwij, 
                                                                  snr = snr, 
                                                                  case = CASES[jj])
                                    rs_executionTime, executionTime, MRE_cm, MRE_cs_sol, MRE_cm_sol, MRE_cm_sol_cs, MRE_worst, MRE_worst_sol, MRE_worst_sol_cs = results
                                    # dic_results['Method'].append('Least Squares')
                                    
                                    # %% Save results
                                    dic_results['Iteration'].append(ss)
                                    
                                    if CASES[jj] == 'Random':
                                        dic_results['Rmin'].append(RMIN)
                                        dic_results['Rmax'].append(RMAX)
                                        dic_results['Rwij'].append(0)
                                        dic_results['Rw'].append(0)
                                    else:
                                        dic_results['Rwij'].append(Rwij)
                                        dic_results['Rw'].append(Rw)
                                        dic_results['Rmin'].append(0)
                                        dic_results['Rmax'].append(0)
                                    
                                    dic_results['Rsmax'].append(Rsmin)
                                    dic_results['Rsmin'].append(Rsmax)
                                    dic_results['SNR'].append(SNR[ii])
                                    dic_results['Case'].append(CASES[jj])
                                    dic_results['Rs Tol'].append(TOLERANCES[zz])
                                    dic_results['CC'].append(CALIBCOLS[bb])
                                    dic_results['Size'].append(SIZES[mm])
                                    dic_results['Time Rs (s)'].append(rs_executionTime)
                                    dic_results['Time (s)'].append(executionTime)
                                    dic_results['MRE cm'].append(MRE_cm)
                                    dic_results['MRE cm LSQR'].append(MRE_cm_sol)
                                    dic_results['MRE cs LSQR'].append(MRE_cs_sol)
                                    dic_results['MRE worst'].append(MRE_worst)
                                    dic_results['MRE worst LSQR'].append(MRE_worst_sol)
                                    
                                    counter = counter + 1
                                    print('Total executed = '+str(fmt.format(100*counter/total_length))+'%')
                                    print('Total time = ',time.time()-startTime,'secs')
                                    print('\n\n\n')
                                    
                                    # %% Save as csv 
                                    print("Saving data in CSV...")
                                    
                                    
                                    dataframe = pd.DataFrame(data = dic_results)
                                    path = os.path.basename(__file__)[:-3]
                                    filename = path+'.csv'
        
                                    # dataframe.to_csv(filename)
        
                                    dataframe_csv = pd.read_csv(filename,index_col=0)
                                    dataframe_csv = pd.concat([dataframe_csv,dataframe],ignore_index=True)
        
                                    dataframe_csv.to_csv(filename)
                                    
                                    key_list = list(dic_results.keys())
                                    for key in key_list:
                                        dic_results[key] = []
                                        
                                    print("\n\n\n")
                                    
            

    print("SIMULATIONS ENDED SUCCESSFULY")

