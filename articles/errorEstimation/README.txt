The file error2D contains the calculations used in the article 

Uncertainty Analysis in the Inverse of Equivalent Conductance Method for Dealing with Crosstalk in 2-D Resistive Sensor Arrays, submitted to IEEE Sensors Journal

They can be found by running in a console (I used linux):

$ python error2D.py

See the main function of error2D.py. The key functions are in the python directory of the code (see file crosstalk.py, which is imported in error2D). 

Error2D.py is just an arrangement of function to call the key functions of crosstalk.py with some data and simulated values, to get the results of the article. 

WARNING: for the 16x16 array I made 1,000 simulations. This may take several days on a regular computer (about 3 days in my computer, Intel(R) Core(TM) i5-4570 CPU @ 3.20GHz, with 4 cores).

Functions: 

simulationRandomError(): a function to compare the noise of the propagation equation with that obtained from a set of simulations adding noise in the computer to an RSA

experimentalRandomError(): a function to compare the noise of the propagation equation with that obtained from a 6x6 real RSA

figures(): plots and saves the figures of the article. It calls in turn several other functions to plot figures. It takes the results of the simulations obtained in the main functions. The file names are hardwired in the code, be careful. 

Original data used are in directory data. Results and figures are written in directory results

On the other hand, the file error2D_appendix.py contains the functions to get the results of the appendix. It is adaptation of IECM to QZPM (Quasi Zero Potential Method).

$ python error2D_appendix.py

During the execution several results are printed on the screen. Values are also saved as .npy files in the results folder.

Note: the word uncertainty should be used instead of error. I haven't changed it yet in file names.



