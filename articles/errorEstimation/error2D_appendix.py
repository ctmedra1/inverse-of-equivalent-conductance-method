'''
Copyright 2019-201 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) and Javier Martinez-Cesteros 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

-----

File to answer rewiever's comments to the article:

Uncertainty Analysis in the Inverse of Equivalent Conductance Method for Dealing with Crosstalk in 2-D Resistive Sensor Arrays, submitted to IEEE Sensors Journal

The file can be run to obtain the results of the appendix.

It compares the adaptation of IECM to QZPM (Quasi Zero Potential Method) and the original method:

Hidalgo et al., A Proposal to Eliminate the Impact of Crosstalk on Resistive Sensor Array Readouts, IEEE Sensors Journal, 2020, doi: 10.1109/JSEN.2020.3005227


'''


import scipy.interpolate
import scipy.linalg
import numpy as np
import sys

sys.path.append('../../python/')

import crosstalk_QZPM_IECM as cr

###
def compare_IECM_QZPM():
    rdir = 'results/'
    import time
    np.random.seed(2021)
    for singleRm in [True, False]:
        print('-------------------------------------')
        for N in range(4, 14, 2):
            NR, NC = N, N
            for deltar in [5.0,1.0]:
                gms = []
                vals1, vals2 = [], []
                t1 = time.time()
                for nsim in range(0,10):
                    gmin, gmax = 1/10e3, 1/100.0
                    gm = gmin+(gmax-gmin)*np.random.rand(NR, NC)
                    rsmin, rsmax = 10.0-deltar, 10.0+deltar
                    if(not singleRm):
                        rs_r = rsmin+(rsmax-rsmin)*np.random.rand(NR)
                        rs_c = rsmin+(rsmax-rsmin)*np.random.rand(NC)
                    else:
                        rs = rsmin+(rsmax-rsmin)*np.random.rand()
                        rs_r = rs*np.ones(NR)
                        rs_c = rs*np.ones(NC)
                    gs_r = 1.0/rs_r
                    gs_c = 1.0/rs_c
                    vref = 1.0
                    rd= 100.0
                    gd = 1/rd
                    rf = 400.0
                    gf = 1/rf
                    ## Experimental values of equivalent conductanes
                    geqExp = cr.geqQZPM(gm, gs_r, gs_c)
                    ## Recover the sensor array using the IECM adaptation
                    gm1, gs_c1, gs_r1, sol = cr.lsqrSolution_QZPM(geqExp, NR, NC, bounds=True, singleRm = singleRm)
                    #import ipdb;ipdb.set_trace()
                    e1 = abs(gm1-gm)/gm
                    vals1.append(e1.flatten())
                    # Consider for instance, rm as a fixed value
                    rsmean = 0.5*(rsmin+rsmax)
                    # Voltage measured in the QZPM circuit. Ideal OA and ADC
                    vqzpm = cr.voltageQZPM(gm, gs_r, gs_c, gd, gf, vref)
                    # Recover sensor values using QZPM
                    gm2 = cr.sensorQZPM(vqzpm, rsmean, rd, rf, vref) 
                    e2 = abs(gm2-gm)/gm
                    vals2.append(e2.flatten())
                    # 
                    gms.append(gm.flatten())
                vals1 = np.concatenate(vals1)
                vals2 = np.concatenate(vals2)
                gms = np.concatenate(gms)
                print('size singleRm deltar IECM (mean, 95, max) QZPM (mean, 95, max)')
                print(N, singleRm, deltar, vals1.mean(), np.percentile(vals1, 95), vals1.max(), vals2.mean(), np.percentile(vals2, 95), vals2.max())
                print('time ', time.time()-t1)
                fname = str(N)+str(singleRm)+str(deltar)+'IECM_ARE.npy'
                np.save(rdir+fname, vals1)
                fname = str(N)+str(singleRm)+str(deltar)+'QZPM_ARE.npy'
                np.save(rdir+fname, vals2)
                #
                fname = str(N)+str(singleRm)+str(deltar)+'_gms.npy'
                np.save(rdir+fname, gms)
    return
###
def figureAppendix():
    import matplotlib.pyplot as plt
    plt.ion()
    #ddir = 'data/'
    rdir = 'results/'
    N = 10
    singleRm = False
    deltar = 5.0
    fname = str(N)+str(singleRm)+str(deltar)+'IECM_ARE.npy'
    a = np.load(fname)
    fname = str(N)+str(singleRm)+str(deltar)+'QZPM_ARE.npy'
    b = np.load(fname)
    #
    fname = str(N)+str(singleRm)+str(deltar)+'_gms.npy'
    gs = np.load(fname)
    plt.clf()
    #plt.semilogy(g, b, marker = 's', markerfacecolor = 'w', markeredgecolor= 'k', linestyle='', markersize = 4)
    #plt.semilogy(gs, a, '+k')
    plt.semilogy(gs, a, '+')
    plt.semilogy(gs, b, '+')
    plt.xlabel('$g_{ip}$ (S)', size = 14)
    plt.ylabel('ARE', size=14)
    plt.grid('on', ls=':')
    plt.savefig(rdir+'QZPMIECM.pdf', dpi = 600)
    return
###
if __name__ == "__main__":
    # Function to run the results of appendix of the article.
    # Then you should run figuresAppendix() to get the figures.
    print('Warning: the simulation takes several hours')
    compare_IECM_QZPM()   
