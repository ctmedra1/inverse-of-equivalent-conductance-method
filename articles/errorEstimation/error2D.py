'''
Copyright 2019-201 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) and Javier Martinez-Cesteros 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------
Internal comments from developers 


Difference with error2D

Clean code
In the simulation, use as g the solution of the algorithm
not as the true g. In a real experiment we do not know 
the true g

Difference with v1

Use crosstalk_v14 (no big differences, just commented and use numpy instead
of scipy for basic functions)
----------
'''


import scipy.interpolate
import scipy.linalg
import numpy as np
import sys

sys.path.append('../../python/')

import crosstalk as cr


def boxCharacter():
    """
    Utility function used to find mux and DAC impedances
    """
    ddir = 'data/'
    Rserie = 2200.0
    print('-----------------------')
    print('Open circuit')
    raw = np.genfromtxt(ddir+'balance_file_Open.txt', delimiter =',',\
     skip_footer= 1, usecols = range(256))
    g = cr.getConductance16x16(raw, Rserie=Rserie, box = None, nbits = 12)
    print(" gstd/gmean ", g.std(), g.mean(), g.std()/g.mean())
    g0 = g.mean(0) # Best approx for each cell: mean in temporal index
    print(" g0std/g0mean ", g0.std(), g0.mean(), g0.std()/g0.mean())
    print("RADC = ", 1/g.mean())
    RADC = 1/g.mean()
    print("Approximate random error in g ", g.std(0).mean())
    print("Not very different from general std ", g.std())
    print("RADC ", RADC)
    ####
    print('----------------------------')
    Rmux = []
    for fname in ['balance_file_short_0_x.txt', 'balance_file_short_x_0.txt']:
        # In the same file we are short circuiting wiht a fixed column and 
        # change row (one file) or the other way round
        print(fname) 
        raw = np.genfromtxt(ddir+fname, delimiter =',', \
          skip_footer= 1, usecols = range(256))
        g = cr.getConductance16x16(raw, Rserie=Rserie, box = None, nbits = 12)
        gwindow = []
        # Select times in which gmax in the mat is higher than 1/200 Ohms
        # (we are short circuiting so g is about 1/Rmux)
        gmax1 = g.max(1) # It will select the short circuit cell
        sel = gmax1 > 1/200.0 
        #dg = np.dfiff(g.max(1))
        if (sel[0]): ii1 = 0 # First window. Start with high g
        for ii in range(1, g.shape[0]-1):
            if(not sel[ii-1] and sel[ii]):
                # start of window
                ii1 = ii
            if(sel[ii-1] and not(sel[ii])):
                #end of window 
                ii2 = ii
                # Append window with some precaution interval
                gwindow.append(gmax1[ii1+5:ii2-5])
                #print(ii1, ii2)
        if(sel[-1]): gwindow.append(gmax1[ii1+5:-1]) # Also ends with high g
        gm = []
        for g in gwindow:
            gm.append(g.mean()) # Add the mean for each cell short circuited
        gm = np.array(gm)
        print('std/mean over cells ', gm.std(), gm.mean(), \
          gm.std()/gm.mean())
        gwindow = np.concatenate(gwindow)
        print('std/mean global ', gwindow.std(), gwindow.mean(),\
         gwindow.std()/gwindow.mean())
        print('Rmux ', 1/gwindow.mean())
        Rmux.append(1/gwindow.mean())
    Rmux = np.array(Rmux)
    Rmux = Rmux.mean()
    print('Rmux final', Rmux)
    # Now do the same for all the known resistors
    # considering box velo3_003 in which I have inserted the values given
    # by previous lines
    # Note the series of 0.1% resistor got the verys similar results. See
    # noise noiseComparison.pdf
    gtrues = []
    gstds = []
    for ii in range(2):
        gstd = []
        if(ii==0):
            Rtrues = [100.2, 1186.0, 3230, 9945.0, 98.3e3, 218e3]
            fnames = ['balance_file_100Ohms.txt', 'balance_file_1.2kOhms.txt', \
             'balance_file_3k3.txt', 'balance_file_10kOhms.txt', \
            'balance_file_100k.txt', 'balance_file_220k.txt']
        else:
            Rtrues = [100.0, 1e3, 1e4, 1e5]
            fnames = ['balance_0.1/balance_file_100_0.1.txt', \
              'balance_0.1/balance_file_1k_0.1.txt', \
              'balance_0.1/balance_file_10k_0.1.txt', \
              'balance_0.1/balance_file_100k_0.1.txt']
        for Rtrue, fname in zip(Rtrues, fnames):
            print('-----------------------------------')
            print(fname)
            raw = np.genfromtxt(ddir+fname, delimiter =',', \
              skip_footer= 1, usecols = range(256))
            g = cr.getConductance16x16(raw, Rserie=Rserie, \
              box = 'velo3_003', nbits = 12)
            g = g[:,15] # The cell with R under test
            gstd.append(g.std())
            print("Value std/mean ", g.std(), g.mean(), g.std()/g.mean())
            Rm = 1.0/g.mean() # Measured with box
            print('Rm ', Rm)
        gtrues.append(1.0/np.array(Rtrues))
        gstds.append(np.array(gstd))
    return gtrues, gstds
####
def noiseModel():
    """
    Using some values from a DAQ system, this function returns a 
    function (from interpolate.interp1d) that gives the relative 
    standard deviation of noise given a conductance.
    The interpolation is done in log scale for the conductance
    Of course, this change from DAQ from DAQ. 
    """
    gtrues = np.array([1e-02, 1e-03, 1e-04, 1e-05, 4.58715596e-06])
    gstd = np.array([7.663307129965109e-05, 1.3419280858789535e-06, \
      2.362832639422214e-07,1.6223024148808994e-07, 1.55886816e-07])
    fint = scipy.interpolate.interp1d(np.log(gtrues), gstd/gtrues, \
      fill_value = (0.034, 0.0077), bounds_error = False)
    return fint
####
def simulationRandomError(outname, NSIM, csheet, case):
    '''
    This function runs a series of simulations, estimates noise and
    compares noise with propagation error equation.
    Results are saved to filne outname. The number of simulation is
    NSIM. Csheet is the parallel resistance of Valostat (between rows
    and columns, set to 0 in this article).
    Case is one of the three cases describes in the article: 'sim' 
    not used', 'sim2' simulated random RSA, 'feet' RSA coming from a
    pressure sensitive mat with feet, 'ghost' similar to feet but in 
    semi-tandem position (thus, a ghost effect was seen in the uncorrec-
    ted pressure image)
    '''
    #import ipdb; ipdb.set_trace()
    ddir = 'data/' # Data directory
    rdir = 'results/' # Results directory
    np.random.seed(2020)
    fint = noiseModel()
    if(case == 'sim'):
        N, M = 16,16
        g = np.random.choice([1e-2, 1e-3, 1e-4, 1e-5], (N,M))
    elif(case == 'sim2'):
        N, M = 16,16
        lgmin = -5
        lgmax = -2
        # Uniform distribution in log scale
        # Used in the article
        lg = lgmin+(lgmax-lgmin)*np.random.rand(N,M)
        g = 10**lg
    elif(case == 'feet'):
        raw = np.loadtxt(ddir+'balanceFeet.txt')
        # Take temporal median
        raw = np.median(raw, 0)
        raw.resize(16,16)
        N, M = raw.shape
        ceq = cr.getConductance16x16(raw, Rserie = 2000.0, nbits=12, \
          box='black')
        # Take next g as the true values
        g = cr.lsqrSolution(ceq, cm0 = None, csheet = csheet, \
            xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    elif(case == 'ghost'):
        raw = np.loadtxt(ddir+'balanceGhostFeet.txt')
        # Take temporal mean
        raw = np.median(raw,0)
        raw.resize(16,16)
        N, M = raw.shape
        ceq = cr.getConductance16x16(raw, Rserie = 2000.0, nbits=12, \
          box='black')
        # Take next g as the true values
        g = cr.lsqrSolution(ceq, cm0 = None, csheet = csheet, \
            xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    else:
        print('unkown case')
        return
    gsol = np.zeros((NSIM, N, M))
    # Get equivalent conductance as it g was perfect even for experiments
    ceq = cr.getCeq(g, csheet = csheet) 
    gsolNominal = cr.lsqrSolution(ceq, cm0 = None, csheet = csheet ,\
        xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    stdrel = fint(np.log(ceq))
    for nsim in range(NSIM):
        #g2 = g + g*stdrel*np.random.randn(N,M)
        ceq2 = ceq+ceq*stdrel*np.random.randn(N,M)
        #gsol[nsim,:,:] = cr.fixedPointSolution(ceq2, csheet = csheet, \
        #  NITER=500, beta = 0.05, bounds = True, ftol = 0.1e-6)
        gsol[nsim,:,:] = cr.lsqrSolution(ceq2, cm0 = None, csheet = csheet, \
            xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    # In an experiment, we only now G 
    # So use gsolNominal to find the derivative and obtain error propagation
    ceqdum, dGdg = cr.getCeqDer(gsolNominal, csheet = csheet)
    dgdG = scipy.linalg.inv(dGdg.reshape(N*M, N*M))
    dG = stdrel.flatten()*ceq.flatten()
    dG.resize(N*M,1)
    dg2 = np.dot(dgdG*dgdG, dG*dG) # Propagated error variance
    # Error variance obtained from simulations
    gsolstd2 = gsol.std(0)**2
    # Save error as std
    res = np.zeros((3,N*M))
    res[0,:] = g.flatten()[:]
    res[1,:] = np.sqrt(gsolstd2.flatten())[:]
    res[2,:] = np.sqrt(dg2.flatten())[:]
    header = case +' '+str(N)+ 'x'+str(M) +' csheet = '+str(csheet)+\
      ' NSIM = '+str(NSIM)
    np.savetxt(rdir+outname, res, header = header)
    return
###
def experimentalError(outname, csheet, case):
    '''
    This function compares the noise in a 6x6 RSA, and
    compares noise with propagation error equation.
    Results are saved to file outname. The input values are stored
    in files whose names are hardwired in the code.
    Case is one of the two cases describes in the article: 'random' 6x6 
    RSA with random values in a set of discrete resistors, 'ghost' 6x6
    RSA with selected values to have a ghost effect 
    '''
    ddir = 'data/'
    rdir = 'results/'
    Rserie = 2200.0
    if(case == 'random'):
        fname = 'balance_file_random6x6.txt'
    elif(case == 'ghost'):
        fname = 'balance_file_ghost6x6.txt'
    else:
        print('unknown case')
        sys.exit(0)
    raw = np.genfromtxt(ddir+fname, delimiter =',', skip_footer= 1, \
      usecols = range(256))
    raw.resize(raw.shape[0], 16, 16)
    # Take only the 6x6 part. 
    raw = raw[:,0:6, 10:]
    geq = cr.getConductance16x16(raw, Rserie=Rserie, box = 'velo3_003',\
      nbits = 12)
    #gsol = np.zeros((100, 6, 6))
    #for nn in range(100):
    gsol = np.zeros(geq.shape)
    for nn in range(0, gsol.shape[0]):
        print(nn)
        gsol[nn,:] = cr.lsqrSolution(geq[nn,:], cm0 = None, csheet = \
          csheet, xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    # Experimental variance
    gsolstd2 = gsol.std(0)**2
    # Theoretical variance (propagation error)
    geqm = np.mean(geq, 0)
    gm = cr.lsqrSolution(geqm, cm0 = None, csheet = csheet, \
      xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    gtrue = gm # 'true' means here the best approx. to values
    geqtrue, dGdg = cr.getCeqDer(gtrue, csheet = csheet)
    dgdG = scipy.linalg.inv(dGdg.reshape(6*6, 6*6))
    fint = noiseModel()
    stdrel = fint(np.log(geqm)) # Use the experimental value as input
    dG = stdrel.flatten()*geqm.flatten()
    dG.resize(6*6,1)
    dg2 = np.dot(dgdG*dgdG, dG*dG)
    res = np.zeros((3,6*6))
    res[0,:] = gtrue.flatten()[:]
    res[1,:] = np.sqrt(gsolstd2.flatten())[:]
    res[2,:] = np.sqrt(dg2.flatten())[:]
    header = case +' '+'6x6' +' csheet = '+str(csheet)
    np.savetxt(rdir+outname, res, header = header)
###
def plotErrorMat(fname):
    '''
    Utility function to print some figures of the article
    '''
    rdir = 'results/'
    res = np.loadtxt(rdir+fname)
    g = res[0,:]
    sdsim = res[1,:]
    sdcal = res[2,:]
    import matplotlib.pyplot as plt
    plt.close('all')
    plt.loglog(g, sdsim/g, marker = 'o', markerfacecolor = 'k', \
        markeredgecolor= 'k', color ='k', linestyle = '')
    plt.loglog(g, sdcal/g, marker = 's', markerfacecolor = 'w', \
        markeredgecolor= 'k', color ='k', linestyle = '')
    # We limit the x-axis to be 1e-8. I think it is more meaningul
    # Numerically we get g closer to 0 (1e-15, 1e-17) but I think
    # it is more interesting to focus on this part of the graph
    x0, x1 = plt.xlim()
    x0 = max(1e-8, x0) # For simulated g (not from PSM), we limit to 1e-8
    plt.xlim(x0, x1)
    y0, y1 = plt.ylim()
    sel = g>1e-8
    y1 = max((sdsim[sel]/g[sel]).max(),(sdcal[sel]/g[sel]).max())
    y1 = 10**(int(np.log10(y1)+1))
    plt.ylim(y0, y1)
    plt.ylabel('$\sigma_g/g$', size = 14)
    plt.xlabel('g (S)', size = 14)
    plt.grid('on', ls =':')
    plt.draw()
    figname = fname[0: fname.find('.')] # remove .txt or .dat ...
    plt.savefig(rdir+figname+'.pdf', dpi = 600)
###
def plotExperimentalError(fname):
    """
    Utility function to print some figures of the article
    """
    rdir = 'results/'
    res = np.loadtxt(rdir+fname)
    g = res[0,:].reshape(6,6)
    sdsim = res[1,:].reshape(6,6)
    sdcal = res[2,:].reshape(6,6)
    import matplotlib.pyplot as plt
    plt.close('all')
    plt.figure()
    ax = plt.gca()
    sel = (g>0.5e-3) # Only 
    inds = np.arange(sel.sum())
    idx = g[sel].argsort()
    g = g[sel][idx]
    sdsim = sdsim[sel][idx]
    sdcal = sdcal[sel][idx]
    rects1 = ax.bar(inds-0.2, sdsim/g, color = '#294463', width = 0.4)
    rects2 = ax.bar(inds+0.2, sdcal/g, color = '#9cc9ff', width = 0.4)
    ax.set_xticks(inds)
    # Set labels as the closest value of resistance 100 or 1k
    # So conductance 1e-3 or 1e-2
    labels = []
    for elem in np.log10(g):
        if (abs(elem+3) < abs(elem+2)):
            labels.append('1e-3')
        else:
            labels.append('1e-2')
    ax.set_xticklabels(labels, rotation = '45')
    ax.set_yscale('log')
    ax.set_ylim(1e-3, 1e-1)
    ax.set_ylabel('$\sigma_g/g$', size = 14)
    ax.legend([rects1, rects2], ['Experimental', 'Error propagation'],\
      loc = 'upper right')
    plt.draw()
    figname = fname[0: fname.find('.')] # remove .txt or .dat ...
    plt.savefig(rdir+figname+'.pdf', dpi = 600)
    return
###
def figures():
    """
    Function to print the figures of the article.
    See the code for output names
    """
    ddir = 'data/'
    rdir = 'results/'
    gtrues = np.array([1e-02, 1e-03, 1e-04, 1e-05, 4.58715596e-06])
    gstd = np.array([7.663307129965109e-05, 1.3419280858789535e-06, \
      2.362832639422214e-07,1.6223024148808994e-07, 1.55886816e-07])
    import matplotlib.pyplot as plt
    import matplotlib.colors
    plt.clf()
    #plt.semilogx(gx, sdrels, 'ok-')
    plt.semilogx(gtrues, gstd/gtrues, 'ok-')
    plt.xlabel('G (S)', size = 14)
    plt.ylabel('$\sigma_G/G$', size = 14)
    plt.grid('on', ls=':')
    plt.draw()
    plt.savefig(rdir+'noiseModel.pdf', dpi = 600)
    ############### Random noise based on random g
    plotErrorMat('sim2.txt') #It is supposed to be in results/ directory
    ############### Random noise based on feet
    plt.clf()
    raw = np.loadtxt(ddir+'balanceFeet.txt')
    # Take temporal median
    raw = np.median(raw, 0)
    raw.resize(16,16)
    N, M = raw.shape
    ceq = cr.getConductance16x16(raw, Rserie = 2000.0, nbits=12, \
      box='black')
    # Take next g as the solution
    csheet = 0.0
    g = cr.lsqrSolution(ceq, cm0 = None, csheet = csheet, \
        xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    plt.imshow(g, vmin = 1e-8, vmax = g.max())
    plt.colorbar()
    plt.savefig(rdir+'imFeet.pdf', dpi =600)
    plotErrorMat('feet.txt')
    ############### Random noise based on ghost feet
    plt.clf()
    raw = np.loadtxt(ddir+'balanceGhostFeet.txt')
    # Take temporal median
    raw = np.median(raw, 0)
    raw.resize(16,16)
    N, M = raw.shape
    ceq = cr.getConductance16x16(raw, Rserie = 2000.0, nbits=12, \
      box='black')
    # Take next g as the solution
    csheet = 0.0
    g = cr.lsqrSolution(ceq, cm0 = None, csheet = csheet, \
        xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
    plt.imshow(g, vmin = 1e-8, vmax = g.max())
    plt.colorbar()
    plt.savefig(rdir+'imGhost.pdf', dpi =600)
    plotErrorMat('ghost.txt')
    ############### Experimental error with random R 6x6
    plt.clf()
    #raw = np.loadtxt('balance_file_random6x6.txt')
    gpcb = np.array([[5,4,5,2,4,5,3,2,4,2,4,3,2,4,5,2,5,2], \
      [3,5,4,2,5,4,5,2,4,2,5,3,5,3,3,3,4,3]])
    gtrue = np.zeros((6,6))
    gtrue[:, 0:3] = gpcb[1,:].reshape(3,6).transpose()
    gtrue[:,3] = gpcb[0,12:]
    gtrue[:,4] = gpcb[0,6:12]
    gtrue[:,5] = gpcb[0,0:6]
    gtrue = 10.0**(-gtrue)
    plt.imshow(gtrue, norm = matplotlib.colors.LogNorm(vmin = 1e-5, vmax = gtrue.max()))
    plt.colorbar()
    plt.savefig(rdir+'imRandom6x6.pdf', dpi =600)
    plotExperimentalError('random6x6.txt')
    ################ Experimental error with ghost R 6x6
    plt.clf()
    gtrue = np.array([[2,2,3,5,5,5],[2,2,3,4,5,3],[4,3,4,4,4,5],\
      [3,3,3,3,4,5],[2,2,4,4,2,2],[2,2,5,5,2,2]])
    gtrue = 10.0**(-gtrue)
    plt.imshow(gtrue, norm = matplotlib.colors.LogNorm(vmin = 1e-5, vmax = gtrue.max()))
    plt.colorbar()
    plt.savefig(rdir+'imGhost6x6.pdf', dpi =600)
    plotExperimentalError('ghost6x6.txt')
    return
###
if __name__ == "__main__":
    # Function to run the main simulations of the article.
    # Then you should run figures() to get the figures.
    print('WARNING: with NSIM = 1,000 it may take several days')
    simulationRandomError(outname='sim2.txt', NSIM=1000, csheet = 0.0, case='sim2')
    #simulationRandomError(outname='sim.txt', NSIM=1000, csheet = 0.0, case='sim')
    simulationRandomError(outname='feet.txt', NSIM=1000, csheet=0.0, case='feet')
    simulationRandomError(outname='ghost.txt', NSIM=1000, csheet=0.0, case='ghost')
    experimentalError(outname='random6x6.txt', csheet=0.0, case='random')
    experimentalError(outname='ghost6x6.txt', csheet=0.0, case='ghost')
   
