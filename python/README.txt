The first code was associated to the article:

C. Medrano et al., "Circuit Analysis of Matrix-Like Resistor Networks for Eliminating Crosstalk in Pressure Sensitive Mats", doi: 10.1109/jsen.2019.2918592, IEEE Sensors Journal 19.18 (2019): 8027-8036

Then several other papers might have been published. A first version of the basic functions in this file was published in oceanCode: doi: https://doi.org/10.24433/CO.7641012.v1

The general methods has been called Inverse of Equivalent Conductance Method.

The code aims to help readers to run the algorithms for their own Resistive Sensor Arrays (RSA).  

1) Resistive sensor array with MUX/DEMUX selection of sensors. Basic hardware configuration. Strong crosstlak. The main routines are available in crosstalk.py.

There are three functions related to the three formal algorithms presented in the first article (see the article for details and the source code for input and output arguments). These algorithms find the true resistance of each cell of an RSA from the resistance measurements of a complete row-column scan  of it. Due to the crosstalk problem, they are not the same (for a full description of the problem see also J. Wu, "Scanning Approaches of 2-D Resistive Sensor Arrays: A Review", doi: 10.1109/JSEN.2016.2641001, IEEE Sensors Journal  17.4 (2017): 914 - 925)

The functions are:
1) fixedPointSolution(): It solves the associated non-linear equation using a general fixed point method. IMPORTANT: the parameter EPSFP established the minimu conductance associated to a node. Originally it was set to 0 (conductances are forced to be nonnegative), but some people reported problems with this (likely due to complete rows or colummns in open circuit from the rest of the network => singular matrix problems). So, instead of zero, set to a very low value. You might check if the value is goog for you: 1e-9 means 1/1e-9 = 1e9 = 1000 MegaOhms, in my case aboe all real resistance when our PSM is under pressure.
2) lsqrSolution(): It solves the problem with a least-squares approach.
3) nonlinearSolution(): It solves the associated non-linear equation using a Newton-Krylov solver included in scipy.

Other functions:

4) getCeqDer(): thus function obtains the equivalent conductance G, given a set of cell conductances, g. It returns also the set of matrix derivatives dGdg, that is, the set of how each element of the equivalent conductance changes for small changes of the input conductance matrix. The inverse of this matrix is the matrix dgdG, that is how a change in the equivalent conductance alters the cell conductance. This matrix can be used in error propagation to estimate the error (uncertainty, random error) of a solution found by any of the algorithms. See testSolutions for an example, the matrix dimensions and how to deal with them and interpret them.

The function solutionsExample() shows their usage. It generates a random 16x16 array, finds the equivalent conductances (the ones that would be obtained in a measurement) and calls the functions to recover the original array. An error performance parameter is also calculated for each algorithm according to the difference with the true conductance, which is known because it is a simulation. Then, it also calculates an uncertainty estimation.

For uncertainty estimation see:
J. Martinez-Cesteros, C. Medrano-Sanchez, I. Plaza-Garcia and R. Igual-Catalan, "Uncertainty Analysis in the Inverse of Equivalent Conductance Method for Dealing With Crosstalk in 2-D Resistive Sensor Arrays," in IEEE Sensors Journal, vol. 22, no. 1, pp. 373-384, 1 Jan.1, 2022, doi: 10.1109/JSEN.2021.3129668.

2) crosstalk_QZPM_IECM.py

This is an adaptation of the IECM to the Quasi Zero Potential Method (QZPM circuit), see the appendix of :

J. Martinez-Cesteros, C. Medrano-Sanchez, I. Plaza-Garcia and R. Igual-Catalan, "Uncertainty Analysis in the Inverse of Equivalent Conductance Method for Dealing With Crosstalk in 2-D Resistive Sensor Arrays," in IEEE Sensors Journal, vol. 22, no. 1, pp. 373-384, 1 Jan.1, 2022, doi: 10.1109/JSEN.2021.3129668.

The function testLsqr_QZPM() shows an example of setting up a QZPM circuit and find the errors when using LSQR to recover sensor conductances.

3) zpm_lsqr.py
The code is associated to the article:

Sergio Domınguez-Gimeno, Carlos Medrano-Sánchez, Raúl Igual-Catalán, Javier Martınez-Cesteros,
Inmaculada Plaza-Garcıa, An Optimization Approach to Eliminate Crosstalk in Zero Potential Circuits for Reading Resistive Sensor Arrays, IEEE Sensors Journal, accepted on May 2023

This article extends a previous method to ZPM circuits:

C. Medrano et al., "Circuit Analysis of Matrix-Like Resistor Networks for Eliminating Crosstalk in Pressure Sensitive Mats", doi: 10.1109/jsen.2019.2918592, IEEE Sensors Journal 19.18 (2019): 8027-8036

The code aims to help readers to run the algorithms for their own Resistive Sensor Arrays (RSA).

The key function is lsqrtSolZpm. In general, given a set of voltages measured in a Zero Potential Circuit, it can retrieve the conductances of the sensors in the array and the switch conductances (ideally zero), see the paper for details. A set of calibration columns with known resistors are required.

In the function solveExperiment there is an example for using the above routine, returning also several results about computing time and errors.
