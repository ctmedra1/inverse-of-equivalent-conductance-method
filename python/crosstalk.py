"""

Copyright 2019-201 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) and Javier Martinez-Cesteros 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

---------
Internal comments from developers
  v13:

  Incorporates the derivatives of equivalent conductance for error estimation,
  sensitivity analysis etc

  getCeqDer

  v14: Just comment a bit more, clean code, add nonlinear solution (Newton-Krylov), change scipy by numpy in some codes
---------

The code is associated to the article:

C. Medrano et al., "Circuit Analysis of Matrix-Like Resistor Networks for
 Eliminating Crosstalk in Pressure Sensitive Mats", 
 doi: 10.1109.jsen.2019.2918592, IEEE Sensors Journal 19.18 (2019): 8027-8036

Further improvements have been made (and may published, see README file).

A first version of the basic functions in this file was published in oceanCode:
    doi: https://doi.org/10.24433/CO.7641012.v1

"""
import scipy as sc
import scipy.misc
import scipy.linalg
import scipy.interpolate
import scipy.stats
import scipy.optimize
import scipy.signal
import os
import sys
import numpy as np


###
def generateNodeEquations(cm, csheet):
    """
    It generates the matrix of node equations taking row 0 as ground
    Inputs:
        cm matrix of conductances between nodes (inverse of resistances)
        csheet sheet conductance between two consecutive conductive lines
          (current flow parallel to velostat sheet) 

    Output:
        If NxM is the dimension of cm, the function returns an 
        (N+M-1) x (N+M-1) matrix.The order of the variables is first 
        row voltages, then column voltages. Row 0 is excluded since it is 0.
        It can be used to solve A V = b where b is a column vector of
        intensities coming from current sources to solve equivalent
        resistance equations.
    """
    N, M = cm.shape
    # N+M-1 nodes (row 0 is ground)
    # First N-1 are row wires, next M are column wires
    A = np.zeros((N+M-1, N+M-1))
    row = 0 # Refers to A
    # Equations of horizontal wires ...
    for ii in range(0,N):
        if(ii==0):
            continue
        A[row,row] = cm[ii,:].sum()
        for column in range(N-1, N-1+M):
            A[row, column] = -cm[ii,column-(N-1)]
        row = row + 1
    # Equations of vertical wires
    for jj in range(0,M):
        column = 0
        for ii in range(0,N):
            if(ii==0):
                continue
            A[row, column] = -cm[ii,jj]
            column = column + 1
        A[row, row] = cm[:,jj].sum()
        row = row + 1
    #print(A.sum())
    #print(A)
    ## Add connections between rows and columns of the matrix resistor
    # A has already been formed (N-1+M x N-1+M   first rows of the resistor array, then columns
    # Row 0 is ground and not included
    # The equations for a node are in a row of A (do not mix A rows with rows of the resistor array, they are different)
    for ii in range(0,N-1):
        # ii = 0 is row 1 etc: remember: node row 0 is ground.
        # So row 0 is not in this loop
        if(ii!=N-2): A[ii,ii] += 2.0*csheet # All but the last are connected to 2 rows
        else: A[ii,ii] += csheet # The last row has only anterior row
        if(ii!=0): A[ii,ii-1] = -csheet # Anterior
        if(ii!=N-2): A[ii,ii+1] = -csheet # Posterior
    for jj in range(0, M):
        column = N-1+jj
        if(jj!=0 and jj!= M-1): A[column, column] += 2.0*csheet # intermediate columns
        else: A[column, column] += csheet # First and last have ony one adjacent column
        if(jj!=M-1): A[column, column+1] = -csheet # Posterior
        if(jj!=0): A[column, column-1] = -csheet # Anterior
    #print(A.sum())
    #print(A)
    return A
##
def getCeq(cm, csheet = 0.0):
    """
    It generates the equivalent conductance matrix, that is the matrix
    of conductances between two nodes (one row and one column).
    Input:
        cm is the matrix of conductances obtained from the resistances that 
         link directly a pair row column (cell resistance)
        csheet sheet conductance between two consecutive conductive lines
    Output:
        It returns a matrix of the same dimension as cm. It is the matrix of
        equivalent conductances
    """
    A = generateNodeEquations(cm, csheet) # Row 0 is ground
    N, M = cm.shape
    # B will contain all the right part of the equations
    # that will allow to get Req between nodes
    # B is a set of column vectors, each one N+M-1
    # The first N-1 refer to row nodes (row 0 is ground so excluded)
    # and the next M elements to column nodes
    # There are N*M columns in B, corresponding to N*M pairs
    # The solution can be obtained in a single call to linalg.solve !!!
    B = np.zeros((N+M-1, N*M))
    bcol = 0 
    for jj in range(0,M):
        # To obtain Req between row node 0 and column node jj
        # the source intensity goes from jj to 0
        # Since row node 0 is not present only a
        # 1 in the vector is present
        b = np.zeros(N+M-1)
        b[N-1+jj] = 1.0
        B[:, bcol] = b[:]
        bcol = bcol +1
    # Now the rest of pairs
    for ii in range(1,N):
        for jj in range(0,M):
            # Req between row node ii and column node jj
            # The source goes from node column jj (+1) 
            # and returns from node row ii (-1)
            # Note that row nodes are placed first in b vector
            b = np.zeros(N+M-1)
            b[ii-1] = -1
            b[N-1+jj] = 1
            B[: ,bcol] = b[:]
            bcol = bcol +1
    #import ipdb; ipdb.set_trace()
    X = sc.linalg.solve(A, B) # X is the voltage solution for each case
    sol2 = np.zeros((N,M))
    for jj in range(0,M):
        # Req between row node 0 and column node jj
        sol2[0,jj] = X[N-1+jj, jj]
    bcol = M
    for ii in range(1,N):
        for jj in range(0,M):
            # Req between column node jj and row node ii
            sol2[ii,jj] = X[N-1+jj, bcol]-X[ii-1, bcol]
            bcol = bcol + 1    
    return 1.0/sol2 # return equivalent conductance
###
def getCeqDer(cm, csheet = 0.0):
    """
    It generates the equivalent conductance matrix, that is the matrix
    of conductances between two nodes (one row and one column).
    It laso generates the matrix of derivatives dGip/dgjk
    Input:
        cm is the matrix of conductances obtained from the resistances that 
         link directly a pair row column (cell resistance)
        csheet sheet conductance between two consecutive conductive lines
    Output:
        It returns a matrix of the same dimension as cm. It is the matrix of
        equivalent conductances
        It also returns the matrix dG of size (N,M,N,M), derivatives dGip/dgjk
    """
    A = generateNodeEquations(cm, csheet) # Row 0 is ground
    N, M = cm.shape
    # B will contain all the right part of the equations
    # that will allow to get Req between nodes
    # B is a set of column vectors, each one N+M-1
    # The first N-1 refer to row nodes (row 0 is ground so excluded)
    # and the next M elements to column nodes
    # There are N*M columns in B, corresponding to N*M pairs
    # The solution can be obtained in a single call to linalg.solve !!!
    B = np.zeros((N+M-1, N*M))
    bcol = 0 
    for jj in range(0,M):
        # To obtain Req between row node 0 and column node jj
        # the source intensity goes from jj to 0
        # Since row node 0 is not present only a
        # 1 in the vector is present
        b = np.zeros(N+M-1)
        b[N-1+jj] = 1.0
        B[:, bcol] = b[:]
        bcol = bcol +1
    # Now the rest of pairs
    for ii in range(1,N):
        for jj in range(0,M):
            # Req between row node ii and column node jj
            # The source goes from node column jj (+1) 
            # and returns from node row ii (-1)
            # Note that row nodes are placed first in b vector
            b = np.zeros(N+M-1)
            b[ii-1] = -1
            b[N-1+jj] = 1
            B[: ,bcol] = b[:]
            bcol = bcol +1
    #import ipdb; ipdb.set_trace()
    X = sc.linalg.solve(A, B) # X is the voltage solution for each case
    sol2 = np.zeros((N,M))
    for jj in range(0,M):
        # Req between row node 0 and column node jj
        sol2[0,jj] = X[N-1+jj, jj]
    bcol = M
    for ii in range(1,N):
        for jj in range(0,M):
            # Req between column node jj and row node ii
            sol2[ii,jj] = X[N-1+jj, bcol]-X[ii-1, bcol]
            bcol = bcol + 1
    geq = 1.0/sol2
    # Obtain derivatives
    dG = np.zeros((N,M,N,M))
    for ii in range(N): # row node indices
        for pp in range(M): # column node indices
            vip = np.zeros(N+M) # include ground in first row
            ix = ii*M+pp
            vip[1:] = X[:,ix] # The rest is the solution
            for jj in range(N):
                for qq in range(M):
                    # jj is the row, qq is the column
                    # First N elements of vip are voltages of rows. the rest columns
                    dG[ii,pp,jj,qq] = (geq[ii,pp]*(vip[jj]-vip[N+qq]))**2
    return geq, dG # return equivalent conductance
###
def genSpice(jj,qq, g, csheet):
    """
    Utility function to test the solution with spice.
    Generates a spice text file
    Circuit used to find equivalent resistance/conductance
    between row jj and column qq given a conductance matrix
    g and a parallel conductance csheet
    """
    N, M = g.shape
    f = open('kkkkqqqq.cir','w')
    f.write('Title \n')
    # Row - column connections
    for ii in range(N):
        if(ii==0): noder = ' 0 '
        else: noder = ' row'+str(ii)+' '
        for pp in range(M):
            nodec = ' col'+str(pp)+' '
            f.write('R'+str(ii)+'_'+str(pp)+noder+nodec+str(1/g[ii,pp])+'\n')
    # Row - row and column-to-column connection
    for ii in range(N-1):
        if(ii==0): noder = ' 0 '
        else: noder = ' row'+str(ii)+' '
        noder1 = ' row'+str(ii+1)+' '
        f.write('RR'+str(ii)+'_'+str(ii+1)+' '+noder+noder1+str(1/csheet)+'\n')
    for pp in range(M-1):
        nodec = ' col'+str(pp)+' '
        nodec1 = ' col'+str(pp+1)+' '
        f.write('RC'+str(pp)+'_'+str(pp+1)+' '+nodec+nodec1+str(1/csheet)+'\n')
    if(jj==0): noder = ' 0 '
    else: noder = ' row'+str(jj)+' '
    nodec = ' col'+str(qq)+' '
    f.write('I '+noder+nodec+'\n') # Source
    f.write('.DC I 1e-3 1.1e-3 0.1\n')
    # .PRINT statement
    prst = '.PRINT DC '
    #for ii in range(1,N):
    # Row 0 is ground. I tried to print it but it didn't work
    # So take into account later
    if(jj!=0): prst += ' V(row'+str(jj)+') '  
    #for pp in range(M):
    prst += ' V(col'+str(qq)+') '
    f.write(prst+'\n')
    f.write('.END\n')    
    f.close()
###
def solveSpice(g, csheet):
    """
    Utility function to test the solution with spice
    It compares our solution with the solution with spice
    Ngspice must be installed.
    g is the conductance martrix. chseet the parallel resistance
    """
    N, M = g.shape
    ceq = np.zeros((N,M))
    #import ipdb;ipdb.set_trace()
    for jj in range(N):
        for qq in range(M):
            genSpice(jj,qq, g, csheet)
            # Solve spice 
            os.system('ngspice -b -o kkkkqqqq.log kkkkqqqq.cir ')
            # Get values
            f = open('kkkkqqqq.log','r')
            s = f.readline()
            while(s!=''):
                if('Index   i-sweep' in s): # I use the start of line to find it
                    s = f.readline() # Next line is -----...
                    s = f.readline()
                    data = np.fromstring(s, sep ='\t')
                    break
                s= f.readline()
            f.close()
            if(jj!=0): vr, vc = data[-2], data[-1] # Last two are the row column
            else: vr, vc = 0, data[-1] # Row 0 is ground and not in spice output
            ceq[jj,qq] = 1e-3/(vc-vr) # 1e-3 A is injected, see genSpice
    return ceq            
###
def testCeq():
    """
    Utility function for testing
    Compare spice and my solution with random arrays
    """
    import numpy as np
    np.random.seed(2021)
    gmin = 1e-5
    gmax = 1e-2
    for nn in range(0,100):
        N = int(2+np.random.rand()*10)
        M = int(2+np.random.rand()*10)
        g = gmin + (gmax-gmin)*np.random.rand(N,M)
        #csheet = gmin*0.1+gmin*0.1*sc.rand()
        # csheet is usally samll but for testing let it be any value
        csheet = gmin + (gmax-gmin)*np.random.rand()
        ceq = solveSpice(g, csheet)
        ceq2 = getCeq(g, csheet)
        if(not np.allclose(ceq, ceq2)):
            print('Solutions are not close')
            #import ipdb; ipdb.set_trace()
            print(N,M)
### 
def testdG():
    """
    Function used to test the derivatives dGdg and dgdG
    Equations are compared with numerical approximations
    of the derivatives.
    The results are good in general, although some high relative 
    differences can be found but for low values (there can be
    more numerical error in approximations).
    Maximum differences between theoretical and numerical derivatives
    are printed
    """
    import numpy as np
    np.random.seed(2021)
    for nn in range(20):
        N = int(2+np.random.rand()*8)
        M = int(2+np.random.rand()*8)
        print(N,M)
        gmin = 1e-4
        gmax = 1e-2
        csheet = gmin + (gmax-gmin)*np.random.rand()
        #csheet = 0.0
        g = gmin + (gmax-gmin)*np.random.rand(N,M)
        geq, dGdg = getCeqDer(g, csheet = csheet)
        # The reshape has the right order
        # First row nodes then column node
        # So the first row of dGdg is
        # dG[row0 col0]/dg[row0 col0] dG[row0 col0]/dg[row0 col1] ....
        # followed in the same line by dG[row0 col0]/dg[row1 col0] ...
        dgdG = scipy.linalg.inv(dGdg.reshape(N*M, N*M))
        #dgdG.resize(N,M,N,M) # This does not give the right order. Why?
        # It seems it is not just undoing a resize 
        for epsilon in [1e-7, 1e-8]:
            dGnum = np.zeros((N,M,N,M))
            dgnum = np.zeros((N,M,N,M))
            for jj in range(N):
                for qq in range(M):
                    gnew = g.copy()
                    gnew[jj, qq] -= epsilon
                    geq1, dG1 = getCeqDer(gnew, csheet = csheet)
                    gnew[jj, qq] += 2*epsilon
                    geq2, dG2 = getCeqDer(gnew, csheet = csheet)
                    dGnum[:,:,jj,qq] = (geq2-geq1)/(2*epsilon)
            dif = abs(dGdg-dGnum)
            difrel = abs((dGdg-dGnum)/dGdg)
            imax = np.unravel_index(dif.argmax(), dif.shape)
            imaxRel = np.unravel_index(difrel.argmax(), difrel.shape)
            print('dG ', epsilon, dif.max(), dGdg[imax])
            print('dG rel', epsilon, difrel[imaxRel], dGdg[imaxRel])
            """
            # Not using allclose because some times we are not 
            # so accurate. It is better to print them
            if(not np.allclose(dGdg, dGnum)):
                print('Solutions are not close dG')
                import ipdb; ipdb.set_trace()
                print(N,M)
            """
            # The other way round. This implies more numerical error from
            # the algorithm because g is not the right one
            #import ipdb; ipdb.set_trace()
            for jj in range(N):
                for qq in range(M):
                    # Using lsqr is more precise. If using fixed point solution, use low ftol
                    geqnew = geq.copy()
                    geqnew[jj,qq] -= epsilon
                    #g1 = fixedPointSolution(geqnew, cm0=None,csheet = csheet, beta = 0.05, NITER = 500, bounds = True, ftol = 0.1e-6) # ftol lower to make it more accurate. It has a clear influence
                    g1 = lsqrSolution(geqnew, cm0 = None, csheet = csheet, xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
                    geqnew[jj,qq] += 2.0*epsilon
                    #g2 = fixedPointSolution(geqnew, cm0=None,csheet = csheet, beta = 0.05, NITER = 500, bounds = True, ftol = 0.1e-6)
                    g2 = lsqrSolution(geqnew, cm0 = None, csheet = csheet, xtol = 1.49012e-08, bounds = True, ftol = 1e-8)
                    dgnum[:,:,jj,qq] = (g2-g1)/(2*epsilon)
                    #print(dgnum[ii,pp,jj,qq], dgdG[ii,pp,jj,qq])
            dgnum.resize(N*M,N*M) # To have the same dimensions as dgdG. It works as expected (w.r.t node order) and can be compared to dgdG
            dif = abs(dgdG-dgnum)
            difrel = abs((dgdG-dgnum)/dgdG)
            imax = np.unravel_index(dif.argmax(), dif.shape)
            imaxrel = np.unravel_index(difrel.argmax(), difrel.shape)
            print('dg ', epsilon, dif.max(), dgdG[imax])
            print('dg rel ', epsilon,  difrel[imaxrel], dgdG[imaxrel])
            """
            # Not using allclose because some times we are not 
            # so accurate. It is better to print them
            if(not np.allclose(dgdG, dgnum)):
                print('Solutions are not close dg')
                import ipdb; ipdb.set_trace()
                print(N,M)
            """
    return
###
def fixedPointSolution(cmeq, cm0=None,csheet = 0.0, beta = 0.05, NITER = 25, bounds = False, ftol = 6e-6):
    """
    It finds the direct conductance of the link between each pair of
    nodes (row-column) given the equivalent conductance of each pair.
   
    It uses a fixed point approach.
   
    Inputs:
        cmeq: equivalenteconductance matrix (measured values)
        cm0: initial guess for the solution. If None, cmeq is taken.
        csheet sheet conductance between two consecutive conductive lines 
        beta: update parameter for iterations
        NITER: maximum number of iterations
        bounds: whether to apply bounds (force conductance to be positive)
        ftol: kind of tolerance similar toscipy.optimize.newton_krylov: 
          f_tol : float, optional. Absolute tolerance (in max-norm) for 
          the residual. If omitted, default is 6e-6.
    Output:
        It returns the value of the cell conductances (same dimension as cmeq)
    """

    EPSFP = 1e-9 # Value used instead of zero for conductances < 0
    if(cm0 is None): c0 = cmeq
    else: c0 = cm0
    #import ipdb; ipdb.set_trace()
    for nn in range(NITER):
        ceq0 = getCeq(c0, csheet= csheet)
        # Termination condition
        # Residual of the initial nonlinear problem
        residual = np.fabs(ceq0-cmeq).max()
        #print('residual', residual)
        if(residual < ftol):
            #print(nn)
            break
        c1 = c0 - beta*(ceq0 - cmeq)
        if(bounds):
            c1[c1<0] = EPSFP
        c0 = c1
    #print(residual)
    return c1
###
def lsqrSolution(cmeq, cm0 = None, csheet = 0.0, xtol = 1.49012e-08, bounds = False, ftol = 1e-8):
    """
    It finds the direct conductance of the link between each pair of 
    nodes (row-column) given the equivalent conductance of each pair.
    Use a least-square approach
    Inputs:
        cmeq is the matrix of equivalent conductances, which is the one 
          that is measured from experiment
        cm0 is an initial value for the algorithm. If None, cmeq is taken
        csheet sheet conductance between two consecutive conductive lines
        bounds boolean variable. If true a call to a function that can 
          deal with bounds is done
        xtol, ftol: tolerance parameters for termination. They are 
          passed to the scipy functions
    Outputs:
        It returns a matrix of site conductances (same dimension as cmeq)
    """
    def fobj(x, cmexp, csheet = csheet):
        # It seems that x is passed as a 1D array, so reshape
        N, M = cmexp.shape
        x2 = x.reshape(N,M)
        #x2 = x.copy()
        cm = getCeq(x2, csheet)
        return (cm-cmexp).flatten() # The square and sum is done by the optimization itself
    # xtol changes the executiom time but ftol has no clear influence
    if(cm0 is None):
        if(not bounds):
            sol = scipy.optimize.leastsq(fobj, cmeq.flatten(), (cmeq,csheet), xtol = xtol, ftol = ftol)
        else:
            sol = scipy.optimize.least_squares(fun = fobj, x0 = cmeq.flatten(), xtol = xtol, bounds = (0.0, np.inf), args = (cmeq,csheet))
    else:
        if(not bounds):
            sol = scipy.optimize.leastsq(fobj, cm0.flatten(), cmeq, xtol = xtol, ftol = ftol)
        else:
            sol = scipy.optimize.least_squares(fun = fobj, x0 = cm0.flatten(), xtol = xtol, bounds = (0.0, np.inf), args = (cmeq,))
    # First element of the solution is the conductance matrix itself
    N, M = cmeq.shape
    if(not bounds):
        cm = sol[0].reshape(N,M)
    else:
        cm = sol['x'].reshape(N,M)
    return cm
###
def nonlinearSolution(cmeq, cm0 = None, csheet = 0.0, f_tol = None):
    """
    It finds the direct conductance of the link between each pair of
    nodes (row-column) given the equivalent conductance of each pair.
    It uses a newton_krylov solver
    Inputs:
        cmeq is the matrix of equivalent conductances, which is the one that is measured from experiment
        cm0 is an initial value for the algorithm. If None, cmeq is taken.
        csheet sheet conductance between two consecutive conductive lines
        ftol: tolerance parameter for termination. They are passed to the scipy functions
    Outputs:
        It returns a matrix of the cell conductances (same dimension as cmeq)
    """
    def F(cm):
        ceq = getCeq(cm, csheet)
        return ceq-cmeq
    if(cm0 is None): cm0 = cmeq
    sol = scipy.optimize.newton_krylov(F, cm0, f_tol = f_tol)
    return sol
###
def getConductance16x16(rawMap, Rserie = 2000.0, nbits = 12, box = None):
    """
    Function for our Velostat Pressure Sensitive Mat.
    It is used to get conductance from the raw value of the ADC in a scan (rawMap)
    """
    R1 = Rserie # Voltage divider resistance (connected to power Vref)
    lsb = 1.0/(2**nbits-1)
    val = rawMap*lsb
    #conductance = (1.0/val-1)/R1
    res = R1/(1.0/val-1)
    if(box=='black'):
        """
        Ajuste de victor, resistencia en KOhmios
        Equivale a tener una R del mux de 145Ohms y una R de entrada del ADC de 123 KOhms
        pcn1 =   1.628e+07;
        pcn2 =  -5.013e+06;
        qcn1 =   -1.29e+05;
        qcn2 =   1.589e+07;
        resfit=((pcn1*resistencia1) + pcn2) / ((resistencia1*resistencia1) + (qcn1*resistencia1) + (qcn2)); //coeficientes cada negra
        #despues del ajuste si el valor es menor  a un 1k me se restan los 145 ohm de offset sino se deja tal cual con el ajuste

        if (resfit<1.0){                                  
            resfit=resistencia1-(145.0/1000.0);
        }
        """
        # Se comprueba que el ajuste es como tener una R serie del MUX y una R entada del ADC de 123 KOmhs
        epsilon = 1e-6 
        rin = 123e3 
        rmux = 145.0
        # evitamos que el ajuste no tenga sentido
        res[res<rmux] = rmux+epsilon
        res[res>rin] = rin-epsilon
        resReal = rin*res/(rin-res)-rmux
    elif (box == 'velo3_002'):
        # Se comprueba que el ajuste es como tener una R serie del MUX y una R entada del ADC de 123 KOmhs
        epsilon = 1e-6 
        rin = 131150.0
        rmux = 126.5
        # evitamos que el ajuste no tenga sentido
        res[res<rmux] = rmux+epsilon
        res[res>rin] = rin-epsilon
        resReal = rin*res/(rin-res)-rmux
    elif (box == 'velo3_003'):
        # Se comprueba que el ajuste es como tener una R serie del MUX y una R entada del ADC de 123 KOmhs
        epsilon = 1e-6 
        rin = 143158.0
        rmux = 131.1
        # evitamos que el ajuste no tenga sentido
        res[res<rmux] = rmux+epsilon
        res[res>rin] = rin-epsilon
        resReal = rin*res/(rin-res)-rmux        
    elif(box is None):
        resReal = res
    else:
        print('box unkown')
        sys.exit(0)
    conductance = 1.0/resReal
    return conductance
###
def solutionsExample():
    # Random conductance matrix. Minimum value = 1e-5, max = 1e-2
    cmin = 1e-5
    cmax = 1e-2
    np.random.seed(2021)
    N, M = 9, 12
    cm = cmin+(cmax-cmin)*np.random.rand(N,M)
    #print(cm.sum())
    # Get equivalent resistance matrix (the "measured" one)
    cmeq = getCeq(cm)
    # All the algorithms should find cm as solution
    # Newton-krylov
    cm2 = nonlinearSolution(cmeq, csheet = 0.0, f_tol = None)
    print('Newton-Krylov: Mean abs relative error ', abs((cm2-cm)/cm).mean())
    # Least squares
    cm2 = lsqrSolution(cmeq, csheet = 0.0, xtol = 1e-8, bounds=True)
    print('Least Squares: Mean abs relative error ', abs((cm2-cm)/cm).mean())
    # Fixed point
    cm2 = fixedPointSolution(cmeq, csheet = 0.0, beta = 0.1, NITER =1000, ftol = 1e-12, bounds = True)
    print('Fixed point: Mean abs relative error ', abs((cm2-cm)/cm).mean())
    ##
    # Uncertainty analysis. How reliable is the conductance obtained?
    # Now, cmeq is the measured conductance (equivalent conductance of the
    # array). Let's suppose cm2 is the solution we have found from cmeq
    # (the measured resistance is cmeq, it is all that we know from
    # the experiment)
    cmeq2, dGdg = getCeqDer(cm2, csheet = 0.0)
    # Usually, we are interested in the inverse matrix, dgdG
    # dGdg[ii,pp, jj, qq] is the partial derivative of  G_{i,p} w.r.t
    # g_{j,q} i, j are rows, p, q columns of the RSA
    # We have to consider all the nodes (rows and columns) in a single
    # vector of nodes. The method reshapes does the right job (tested)
    # First all rows nodes, the all column nodes
    dgdGdum = scipy.linalg.inv(dGdg.reshape(N*M, N*M))
    # However, dgdGdum is not so easy to change to other physical indices
    # related to rows and columns of the RSA. I do this in a loop:
    dgdG = np.zeros((N,M,N,M))
    for ii in range(N):
        for pp in range(M):
            for jj in range(N):
                for qq in range(M):
                     r = ii*M+pp
                     c = jj*M+qq
                     dgdG[ii,pp,jj,qq] = dgdGdum[r, c]
    # Now dgDG represents the sensibility of the solution to
    # the measured conductances: dgDG[ii,pp,jj,qq] is the derivative of
    # g[ii,pp] (physical row ii and column jj of the RSA) w.r.t
    # G[jj,qq] (the measured conductance between row jj an column qq)
    ##
    # dgdG can be used to estimate the uncertainty in g if we know the
    # uncertainty in the measured resistance.
    def noiseModel():
        """
        This returns a function to calculate 
        the std of the relative noise as an example
        Some points are measured, the rest interpolated in log scale
        Let us assume that we have a model for the uncertainty 
        in the measured conductance. This function corresponds to the 
        measured noise in a DAQ system of a 6x6 RSA. For another RSA this
        should change and be measured by the user
        """
        gtrues = sc.array([1e-02, 1e-03, 1e-04, 1e-05, 4.58715596e-06])
        gstd = sc.array([7.663307129965109e-05, 1.3419280858789535e-06, \
          2.362832639422214e-07,1.6223024148808994e-07, 1.55886816e-07])
        fint = scipy.interpolate.interp1d(sc.log(gtrues), gstd/gtrues, \
          fill_value = (0.034, 0.0077), bounds_error = False)
        return fint
    #
    fint = noiseModel()
    # Standard deviation of the measurement in relative units
    # We could also use cmeq2. Not sure which is better of if there
    # is some large difference. In any case, cmeq is the measured conductance
    stdrel = fint(np.log(cmeq)) 
    dG = stdrel*cmeq # Error in the measurement
    # Which is the error of the estimated conductance at node ii, pp?
    # It is done in a loop to make it clear, but it could be done more
    # efficiently
    ii = 2
    pp = 3
    dg2_iipp = 0.0
    for jj in range(N):
        for qq in range(M):
            dg2_iipp += dgdG[ii,pp,jj,qq]*dgdG[ii,pp,jj,qq]*dG[jj,qq]*dG[jj,qq]
    print('Estimated uncertainty (std) at cell ',ii,pp, np.sqrt(dg2_iipp))
    # Do it in a short way
    dGkk = dG.flatten()
    dGkk.resize(N*M,1)
    dgdGkk = scipy.linalg.inv(dGdg.reshape(N*M, N*M))
     # Efficient matrix mutiplication to do the same sums as in the loop above
    dg2kk = sc.dot(dgdGkk*dgdGkk, dGkk*dGkk)
    print('Estimated uncertainty (short way) at cell ',ii,pp, np.sqrt(dg2kk[ii*M+pp]))
    
