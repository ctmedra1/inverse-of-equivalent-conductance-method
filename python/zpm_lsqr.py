# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2023 Sergio Dominguez-Gimeno (sdominguezg@unizar.es),
Carlos Medrano-Sanchez (ctmedra@unizar.es, ctmedra@gmail.com) and
Javier Martinez-Cesteros (javi.mz.cs@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


This code is associated to the following article:

Sergio Domı́nguez-Gimeno, Carlos Medrano-Sánchez, Raúl Igual-Catalán, Javier Martı́nez-Cesteros,
Inmaculada Plaza-Garcı́a, An Optimization Approach to Eliminate Crosstalk in Zero Potential Circuits for Reading Resistive Sensor Arrays, IEEE Sensors Journal, accepted on May 2023


This program includes functions to remove crosstalk in Zero Potential Methods in Resistive Sensor Arrays.

"""

import scipy as sc
import scipy.misc
import scipy.linalg
import scipy.interpolate
import scipy.stats
import scipy.optimize
import scipy.signal
import os
import matplotlib.pyplot as plt
# import sys
import numpy as np
import time

# %% GLOBAL VARIABLES
XTOL = 1.49012e-8
FTOL = 1.49012e-8
GTOL = 1.49012e-8
RMIN = 100 #Minimum resistance in the RSA
CMAX = 1/RMIN
RMAX = 500 #Maximum resistance in the RSA
CMIN = 1/RMAX
VCC = 5
RF = 75
fmt = "{:.8g}"
np.set_printoptions(formatter = {'float':fmt.format})

# %% FUNCTIONS
"""
PRIVATE FUNCTIONS
"""
def rmse(V1,V2):
    """
    It calculates RMSE between two voltages
    Inputs:
        V1, V2: two voltages (vectors)
    Output:
        the RMSE value
    """
    return np.sqrt(np.sum((V1-V2)**2))

def awgn(signal,snr):  #based on https://stackoverflow.com/questions/14058340/adding-noise-to-a-signal-in-python
    """
    This function adds noise to a signal. The noise is characterized by 
    its SNR
    Inputs:
        signal: the input signal
        snr: the SNR ratio of the noise (in dB)
    Output:
        signal_noise: the input signal plus a random noise. It is a 1D 
        vector because internally the input is flattened.
    """
    fsignal = signal.flatten()
    N = len(fsignal)
    signal_noise = np.zeros(fsignal.shape)
    for ii in range(N):
        noise_amp = np.fabs(fsignal[ii]*10**(-snr/20))
        noise_std = noise_amp/(np.sqrt(2))#amplitude to rms
        noise = np.random.normal(0,noise_std) #the RMS amplitude equals the standard deviation of the distribution
        signal_noise[ii] = fsignal[ii]+noise
    return signal_noise
    # return signal + noise*signal

def getCmatrix(cm,cs):
    """
    It generates the matrix for a ZPM circuit to solve it using Kirchoff's 
    laws. The matrix is associated to a linear system of equations and
    has units of conductance.
    
    Input:
        cm: conductances of the array. 
        Calibration resistances have to be included. It is a matrix 
        (let it be NxM: N= no. rows, M = no. columns including calibration
        ones). In fact this function does not need to know which columns
        in cm are the calibration ones. 
        cs: conductances of the switches. It is a 1D vector (N+M elements). 
        The first N conductanes are associated to rows, the next M are 
        associated to the column switches
    Output:
        C: The conductance matrix (N+M) x (N+M) associated to the nodes
        in the circuit.
    """
    N,M = cm.shape #here cm includes calibration resistances
    #print(cm)
    C = np.zeros((N+M,N+M))
    #Elements in main diagonal
    for ii in range(0,N):
        C[ii,ii] = cm[ii,:].sum() + cs[ii] #for rows (1,2,3,...)
    for jj in range(0,M):
        C[jj+N,jj+N] = cm[:,jj].sum() + cs[jj+N] #for rows (A,B,C,...)
            
    #The rest of the elements
    for ii in range(0,N):
        for kk in range(0,M):
            #Out of the main diagonal
            C[ii,N+kk] = -cm[ii,kk]
            C[N+kk,ii] = -cm[ii,kk]
                  
    return C

def getImatrix(cs,N,M,Mcc = None):
    """
    It returns the current when applying Kirchoff's law to the ZPM
    circuit
    Inputs:
        cs: vector of switch conductances (first row, then column)
        N: Number of rows of the resistive sensor array
        M: number of columns (excluding calibration ones)
        Mcc: None or number of calibration columns
    Output:
        I: the current part of node equations. Depending of the row 
        connected to power, the current part of the equation is different.
        This function returns all the cases together, Thus, I is 
        organized as a matrix.
        There are N+M or N+M+Mcc nodes in the circuit (depending on Mcc)
        and N different cases. So I is (N+M)xN or (N+M+Mcc)xN depending
        on Mcc.
    """
    if(Mcc == None):
        I = np.zeros((N+M,N))
        for ii in range(0,N):
            I[ii,ii] = VCC*cs[ii] #independent term is conductance*Vcc
    else:
        I = np.zeros((N+M+Mcc,N)) #first Mcc columns are for calibration resistances
        for ii in range(0,N):
            I[ii,ii] = VCC*cs[ii] #independent term is conductance*Vcc
            
    return I  

def getOutputVoltages(V,cs,N,M,Mcc = None):
    """
    It gets the output voltage of a ZPM circuit given the voltages at the nodes
    The voltages are obtained at the output of an opamp.
    
    Inputs:
        V: voltages at the nodes of the circuit. The column voltages start
        at index N
        cs: conductances of switches. The column switches start at index N
        N, M: Number of rows and columns of the resistive sensor array
        (excluding calibration columns).
        Mcc: None or number of calibration columns
    Output:
        Vo: Output voltage at the opamp. It is a NxM or Nx(M+Mcc) matrix
        The size corresponds to the number of pairs row-column selected        
    """
    if Mcc == None:
        Vo = np.zeros((N,M))
        
    else:
        Vo = np.zeros((N,M+Mcc))
                
    Vo = -RF*cs[N:].flatten()*V[N:,:].T
    return Vo

def solveCircuit(cm,cs,cc = None): #return node voltage results
    """
    It solves the node equations for a given circuit
    Inputs:
        cm: conductance matrix of the array
        cs: conductance of switches (first row then columns)
        cc: None or conductances of calibration columns
    Output:
        Vo: voltage  at the output for each row-column selected. It is
        given as a matrix corresponding to the number of rows and columns
    """
    #Configure circuit matrix
    N,M = cm.shape
        
    if(cc is None): #Rs known
        # cm2 = cm
        C = getCmatrix(cm,cs) #concatenate calibration resistances on left
        I = getImatrix(cs,N,M)
        V = sc.linalg.solve(C,I,
                            sym_pos = 'True',
                            assume_a='sym') # X is the voltage solution for each case
        Vo = getOutputVoltages(V,cs,N,M)
        
    else: #Rs unknown
        Ncc,Mcc = cc.shape
        cm2 = np.concatenate((cc,cm),axis=1) #matrix of conductances now include cc
        C = getCmatrix(cm2,cs) #concatenate calibration resistances on left
        I = getImatrix(cs,N,M,Mcc)
        V = sc.linalg.solve(C,I,
                            sym_pos = 'True',
                            assume_a='sym') # X is the voltage solution for each case
        Vo = getOutputVoltages(V,cs,N,M,Mcc)

    return C,V,I,Vo

# %% NUMERIC ALGORITHMS TO OBTAIN CM
def lsqrtSolZpm(N, M, Voeq, Rs_known, cc = None, cs = None, csheet = 0.0, xtol = XTOL, ftol = FTOL, gtol = GTOL, bounds = None, ):
    """
    The main goal is to find the resistances of a resistive sensor array
    with ZPM circuit. It can obtain the switch resistances too.
    It poses the problem using an optimization approach.
    Inputs:
        N, M: Number of rows and colums of the resistive sensor array 
        (excluding calibration columns)
        Voeq: Measured voltage of the ZPM circuit
        Rs_known: boolean variable (whether or not switch resistances are
        known)
        cc: None or conductances of calibration matrix. The number of calibration
        columns is the number of columns of cc.
        cs: None or switch conductances (first rows, then columns)
        csheet: Not used. Set to 0 by default.
        xtol, ftol gtol: internal parameters of the LSQR routine
        bounds: If None, the bounds are 0-infinity for conductance. 
        Otherwise, a list [Rmin, Rmax, Rsmin, Rsmax] with minimum and 
        maximum values of the sensor array and swicthes respectively.
    Output:
        solutions: If Rs_known = True, return the sensor conductance matrix
        NxM. If Rs_known = False, return a flattened array (first
        sensor conductances, then switch conductances). The sensor conductances
        is an NxM array, which is then flattened. The switch conductances
        are flattened to an N+M+Mcc vector in that order (first rows, 
        the columns). Mcc is the number of calibration columns, which can
        be obtained from the shape of cc.
    """
    def fobj_rs(x, Voeq_ext, cc): #objective function: ||Vo-V(g)||². RS IS KNOT KNOWN, SO IT IS NECESSARY TO ESTIMATE IT
        cm2= x[0:N*M]#.reshape(N,M) #first N*M elements correspond to the C matrix' values
        cm2 = cm2.reshape(N,M) #cm recalculated
        cs2 = x[N*M:].copy()#.reshape(N+M+inc_M,1) #cs is at the end of x
        C,V,I,fVo = solveCircuit(cm2,cs2,cc) #voltages readen from estimated circuit (fVo = V(g))
        return (fVo-Voeq_ext).flatten() # fVo is what I need, only for unknown resistances
    
    def fobj(x, Voeq, cs): #objective function: ||Vo-V(g)||². RS IS KNOWN
        # It seems that x is passed as a 1D array, so reshape
        # x is the conductances in the circuit to obtain (varied through lsqr method)
        N, M = Voeq.shape
        x2 = x.reshape(N,M)
        C,V,I,fVo = solveCircuit(x2, cs) #voltages readen from estimated circuit (fVo = V(g))
        return (fVo-Voeq).flatten() # fVo is what I need
    
    Rmin, Rmax, kk, kk = bounds
    cmin = 1/Rmax
    cmax = 1/Rmin
    cm_init = np.mean([cmin,cmax])*np.ones((N,M)) #initial value for cm
    
    startTime = time.time()
    
    #Rs is not a variable
    if Rs_known == 'Yes':
        if(bounds is None):
            sol = scipy.optimize.leastsq(fobj, cm_init.flatten(), (cm_init,csheet),bounds = (0.0,np.inf))
        else:
            Rmin, Rmax, kk, kk = bounds
            cm_ub = 1/Rmin*np.ones(cm_init.shape) #upper bound for cm
            cm_lb = 1/Rmax*np.ones(cm_init.shape) #lower bound for cm

            ub = cm_ub.flatten()
            lb = cm_lb.flatten()
            bnds = [lb,ub] #bounds for each variable to iterate (all resistances)
            
            sol = scipy.optimize.least_squares(fun = fobj, 
                                               x0 = cm_init.flatten(), 
                                               bounds = bnds,
                                               xtol = xtol,
                                               verbose = 1,
                                               args = (Voeq,cs,))
            print('Optimized.')
        
        if(bounds is None):
            cm_lsqr = sol[0].reshape(N,M)
        else:
            cm_lsqr = sol['x'].reshape(N,M)
            
        solutions = cm_lsqr
            
    #Rs is a variable
    elif Rs_known == 'No':
        Rmin, Rmax, Rsmin, Rsmax = bounds
        Ncc, Mcc = cc.shape

        Voeq_ext = Voeq #Voeq contains output from calibration columns as well
        Voeq = Voeq_ext[:,Mcc:Mcc+M]
        csmin = 1/Rsmax
        csmax = 1/Rsmin
        cs_init = np.mean([csmin,csmax])*np.ones((N+M+Mcc,1))
        
        if bounds is None:
            cm_cs = np.concatenate(cm_init.flatten(),cs_init.flatten())
            sol = scipy.optimize.leastsq(fobj_rs, cm_cs, (Voeq_ext,cc,csheet), xtol = xtol, ftol = ftol, bounds = (0.0,np.inf))
        else:
            #define bounds. All bounds come organized in variable bound
            cm_cs = np.concatenate((cm_init.flatten(),cs_init.flatten()))
            cm_ub = 1/Rmin*np.ones(cm_init.shape) #upper bound for cm
            cm_lb = 1/Rmax*np.ones(cm_init.shape) #lower bound for cm
            cs_ub = 1/Rsmin*np.ones(cs_init.shape) #upper bound for cs
            cs_lb = 1/Rsmax*np.ones(cs_init.shape) #lower bound for cs
            
            ub = np.concatenate((cm_ub.flatten(),cs_ub.flatten()))
            lb = np.concatenate((cm_lb.flatten(),cs_lb.flatten()))
            bnds = [lb,ub] #bounds for each variable to iterate (all resistances)
            sol = scipy.optimize.least_squares(fun = fobj_rs, x0 = cm_cs, 
                                               xtol = xtol, 
                                               ftol = ftol, 
                                               gtol = gtol, 
                                               loss = 'soft_l1',
                                               f_scale = 0.0001,
                                               bounds = bnds,
                                               verbose = 1,
                                               args = (Voeq_ext,cc,))
            # sol = scipy.optimize.least_squares(fun = fobj_rs, x0 = cm_cs, xtol = xtol, bounds = bnds, args = (cmeq,csheet))
            print('Optimized.')
        
        if bounds is None:
            cm_lsqr = sol[0][0:N*M].reshape(N,M)
            cs_lsqr = sol[0][N*M:].reshape(N+M+Mcc,1)
        else:
            cm_lsqr = sol.x[0:N*M].reshape(N,M)
            cs_lsqr = sol.x[N*M:].reshape(N+M+Mcc,1)
            
        solutions = np.concatenate((cm_lsqr.flatten(),cs_lsqr.flatten()))
            
        # return cm_lsqr, cs_lsqr, executionTime
    executionTime = (time.time() - startTime) #time that the algorithm has used
    return executionTime, solutions

#%% SPICE FILES GENERATION
def genSpiceZpm(cm,cs,row):
    """
    Generate Spice file for a given circuit for testing out solutions 
    with a different solver
    Inputs:
        cm: conductance matrix of the array. For this function it is not
        important if calibration columns are included. It generates a 
        spice file for a general cm array.
        cs: switch resistances (first rows, the columns)
        row: the row to which power is connected.

    """
    N, M = cm.shape
    f = open('zpm_ngspice_rs_'+str(row)+'.cir','w')
    f.write('PSM_'+str(row)+' \n')
    # Row - column connections
    for ii in range(N):
        noder = ' row_'+str(ii+1)+' '
        for pp in range(M):
            nodec = ' col_'+chr(pp+97)+' '
            f.write('R'+str(ii+1)+'_'+chr(pp+97)+noder+nodec+str(1/cm[ii,pp])+'\n')
            
    #Rs: switches resistances
    #For rows
    for ii in range(N):
        if ii == row: #in the current row, Vcc is placed and so rs does
            second_node = 'row_s'+str(ii+1)
        else:
            second_node = '0'
        f.write('Rs_'+str(ii+1)+' '+'row_'+str(ii+1)+' '+second_node+' '+str(1/cs[ii,0])+'\n')
    
    #For columns
    for jj in range(M):
        f.write('Rs_'+chr(jj+97)+' '+'col_'+chr(jj+97)+' 0 '+str(1/cs[jj+N,0])+'\n') #column's Rs are always to GND
            
    voltage_row = str(row+1) #which row is connected to Vcc
    f.write('Vcc row_s'+voltage_row+' 0 '+str(VCC)+'\n') # Source
    f.write('.tran 1m 0.1\n') #start DC simulation from 0 to 1 at 0.1
    f.write('.END\n')    
    f.close()
    return

def solveSpiceZpm(cm,cs,cc = None):
    """
    It solves the spice circuit generated by genSpiceZpm to which
    the parameters cm and cs are forwarded
    cc: None or matrix corresponding to calibration columns
    """
    N,M = cm.shape
    if cc is None:
        for ii in range(N):
            genSpiceZpm(cm,cs,ii)
            os.system('ngspice -a zpm_ngspice_rs_'+str(ii)+'.cir')
    else:
        for ii in range(N):
            genSpiceZpm(np.concatenate((cc,cm),axis=1),cs,ii)
            os.system('ngspice -a zpm_ngspice_rs_'+str(ii)+'.cir')

# %% MAIN FUNCTION: EXAMPLE
def solveExperiment(N, M, cm, cs, cc, Rsmin, Rsmax, mth = None, Rw = None, Rwij = None, snr = None, case = None):
    """
    It solves the problema for a given configuration.
    Input:
        N, M: number of rows and columsn of the RSA (excluding calibration
        columns)
        cm: conductance of the sensor array (NxM)
        cs: switch conductances (first row N, then column M+Mcc)
        cc: conductance of calibration matrix (NxMcc) matrix
        The number of calibration columns, Mcc, can be obtained from 
        the shape of cc.
        Rsmin: Minimum value of the switch resistances
        Rsmax: Maximum value of the switch resistances
        mth: None
        Rw: For worst case, resistance value of the sensors (except one, Rwij)
        Rwij: For worst case, resistance value of the sensor under study
        snr: Signal to Noise Ratio in dB
        case: 'Normal' or 'Worst'. See article 
        Worst case is called Single Target Resistor Scenario in the last
        version
    Output:
        rs_executionTime: execution time to get both RSA and switch values
        executionTime: execution time to get only RSA values
        MRE_cm: Mean Relative Error of the sensor values provided by ZPM
        MRE_cs_sol: Mean Relative Error of the sensor values obtained by our method
        MRE_cm_sol: Mean Relative Error of the sensor values provided by our method
        MRE_cm_sol_cs: Not used 
        MRE_worst: Mean Relative Error of Rwij obtained by ZPM  (if case is 'Normal', 0)
        MRE_worst_sol: Mean Relative Error of Rwij obtained by our method (if case is 'Normal', 0)
        MRE_worst_sol_cs: Not used
    """
    # N, M, cm, cc, cs, Rsmin, Rsmax, Rs_known = RS_KNOWN[ll], Rw = Rw, Rwij = Rwij, snr = snr, case = CASES[jj]
    # -- GENERATE NODE SYSTEM FOR ZPM AND GET OUTPUT VOLTAGE -- 
    
    MRE_cm = 0
    MRE_cm_sol = 0
    MRE_cm_sol_cs = 0
    MRE_cs_sol = 0 #No calibration resistances needed if Rs is known
    MRE_worst_sol = 0
    MRE_worst_sol_cs = 0

#%% OBTAIN OUTPUT VOLTAGES FROM DATA
    Ncc, Mcc = cc.shape
    # cs_ideal = np.mean([1/Rsmax,1/Rsmin])*np.ones((N+M,1))
    C,V,I,Voeq_ext = solveCircuit(cm,cs,cc)
    # Voeq_ext = Voeq #select only non-calibration columns
    Voeq = Voeq_ext[:,Mcc:Mcc+M]
    cmeq = -Voeq/(VCC*RF) #output for only sensor resistances
    
    # %% CALIBRATION OF RS VALUES
    print("Obtaining aproximation for Rs...")
    if case == "Normal":
        rs_executionTime, solutions = lsqrtSolZpm(N, M, Voeq_ext, 
                                               Rs_known = 'No', 
                                               xtol = np.finfo(float).eps,
                                               ftol = np.finfo(float).eps,
                                               gtol = np.finfo(float).eps,
                                               cc = cc,
                                               bounds = np.array([RMIN,RMAX,Rsmin,Rsmax])) #obtain cs from the interative algorithm
    elif case == 'Worst':
        rs_executionTime, solutions = lsqrtSolZpm(N, M, Voeq_ext, 
                                               Rs_known = 'No', 
                                               cc = cc, 
                                               xtol = np.finfo(float).eps,
                                               ftol = np.finfo(float).eps,
                                               gtol = np.finfo(float).eps,
                                               bounds = np.array([0.9*Rw,1.1*Rwij,Rsmin,Rsmax])) #obtain cs from the interative algorithmbounds = np.array([0.9*Rw,1.1*Rwij,Rsmin,Rsmax]))
    
    cs2 = solutions[N*M:].reshape(N+M+Mcc,1)
    cm2 = solutions[:N*M].reshape(N,M)
    MRE_cs_sol = 100*np.mean(np.abs(cs2-cs)/cs) #Error for estimated Rs
    MRE_cm_sol = 100*np.mean(np.abs(cm2-cm)/cm) #Error for estimated cm
    if(case == 'Worst'): #if worst case is selected 
        i = int(N/2) #i index for the bigger resistor
        j = int(M/2) #j index for the bigger resistor
        MRE_worst = 100*np.abs(cm[i,j]-cmeq[i,j])/cm[i,j]
        MRE_worst_sol = 100*np.abs(cm[i,j]-cm2[i,j])/cm[i,j]
        
    cs = np.concatenate((cs[:N],cs[-M:]))
    cs2 = np.concatenate((cs2[:N],cs2[-M:]))
    C,V,I,Voeq = solveCircuit(cm,cs)
    if(snr is not None): #in case that it's desired, add white gaussian noise
        Voeq = awgn(Voeq,snr)
        Voeq = Voeq.reshape(N,M)
        
    
    # %% GENERATE SPICE (uncomment to use)
    # print('------ Spice Original ------')
    # if Rs_known == 'Yes':
    #     solveSpiceZpm(cm,cs)
    # else:
    #     solveSpiceZpm(cm,cs,cc)
    
    # -- EXTRACT CM FROM OUTPUT VOLTAGES --   
    
    # print('cm = ', cm)
    # print('cm-1 = ',cm**-1)
    # print('cmeq = ', cmeq)
    
    # %% CALCULATE CM WITH PREVIOUSLY OBTAINED RS
    print('Optimizing...')
    if case == 'Normal':
        if mth == 'lsqr':
            executionTime, solutions = lsqrtSolZpm(N, M, Voeq, 
                                                   Rs_known = 'Yes', 
                                                   cs = cs2, 
                                                   bounds = np.array([RMIN,RMAX,Rsmin,Rsmax]))
        
    else: #Worst case
        # if Rs_known == 'Yes':
        if mth == 'lsqr':
            executionTime, solutions = lsqrtSolZpm(N, M, 
                                                   Voeq, 
                                                   Rs_known = 'Yes', 
                                                   cs = cs2,
                                                   bounds = np.array([0.9*Rw,1.1*Rwij,Rsmin,Rsmax]))
    
    # %% PROCESS RESULTS
    cm_sol = solutions
    C_sol,V_sol,I_sol,Voeq_sol = solveCircuit(cm_sol,cs2)
    Voeq_sol = getOutputVoltages(V_sol,cs2,N,M)
        
    print('VO_ERROR = ', rmse(Voeq_sol,Voeq))
    
    # -- MEAN RELATIVE AND ABSOLUTE ERROR FOR ALL RESISTORS --
    MRE_cm = 100*np.mean(np.abs(cmeq-cm)/cm)
    MRE_cm_sol_cs = 100*np.mean(np.abs(cm_sol-cm)/cm)

    if(case == 'Worst'): #if worst case is selected 
        i = int(N/2) #i index for the bigger resistor
        j = int(M/2) #j index for the bigger resistor
        MRE_worst_sol_cs = 100*np.abs(cm[i,j]-cm_sol[i,j])/cm[i,j]
        
    else:
        MRE_worst = 0
        MRE_worst_sol_cs = 0
    
    #Display data
    print('MRE_cm = ', fmt.format(MRE_cm),'%')
    print('MRE_sol_cs = ', fmt.format(MRE_cs_sol),'%')
    print('MRE_sol_cm = ', fmt.format(MRE_cm_sol),'%')
    print('MRE_sol_cm_cs = ', fmt.format(MRE_cm_sol_cs),'%')
    
    print('MRE_worst = ', fmt.format(MRE_worst),'%')
    print('MRE_worst_sol = ', fmt.format(MRE_worst_sol),'%')
    print('MRE_worst_sol_cs = ', fmt.format(MRE_worst_sol_cs),'%')
    print('Elapsed time for Rs = ',rs_executionTime,'secs')
    print('Elapsed time with Rs known = ',executionTime,'secs')
    
    
    return rs_executionTime, executionTime, MRE_cm, MRE_cs_sol, MRE_cm_sol, MRE_cm_sol_cs, MRE_worst, MRE_worst_sol, MRE_worst_sol_cs
    
    
