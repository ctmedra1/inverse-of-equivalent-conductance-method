"""

Copyright 2019-201 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) and Javier Martinez-Cesteros 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

---------

Version to answer rewiever's comments to the article:

Uncertainty Analysis in the Inverse of Equivalent Conductance Method for Dealing with Crosstalk in 2-D Resistive Sensor Arrays, submitted to IEEE Sensors Journal

The file contains functions related to the adaptation of IECM to QZPM (Quasi Zero Potential Method):

Hidalgo et al., A Proposal to Eliminate the Impact of Crosstalk on Resistive Sensor Array Readouts, IEEE Sensors Journal, 2020, doi: 10.1109/JSEN.2020.3005227

"""
import scipy as sc
import scipy.misc
import scipy.linalg
import scipy.interpolate
import scipy.stats
import scipy.optimize
import scipy.signal
import os
import sys
import numpy as np


def generateCommonMatrixG(gm, gs_r, gs_c, A):
    """
    Generates a part of the matrix associated to Kirchoff's law that is
    the same for all the options of nodes connected to the connecting
    node
    
    Kirchoff's law is expressed as AV = I in matrix form
    
    gm: is an array of sensor conductances
    gs_r: internal resistance of switches that select rows
    gs_c: internal resistance of switches that select columns
    A: matrix to be filled. It has dimensions of conductance
    """
    # First voltages of row variables, then column voltages
    NR, NC = gm.shape
    #print(NR, NC)
    for ii in range(NR+NC):
        # row ii of the Kirchoff's law: first row voltages (NR) then column
        # voltages
        for jj in range(NR+NC):
            #print(ii,jj)
            if(ii<NR and ii==jj):
                # Coefficient of row voltage for row node
                A[ii,jj] = gm[ii,:].sum()+gs_r[ii]
            elif(ii<NR and jj>=NR):
                # Coefficient of column voltage for row node
                A[ii,jj] = -gm[ii,jj-NR]
            elif(ii>=NR and jj<NR):
                # Coefficient of row voltage for column node
                A[ii,jj] = -gm[jj,ii-NR]
            elif(ii>=NR and jj==ii):
                # Coefficient of column voltage for column node
                A[ii,jj] = gm[:,ii-NR].sum()+gs_c[ii-NR]
    return
##
def generateNodeEquations(gm, gs_r, gs_c, row, col, col2, opamp, gd, A):
    """
    It generates the matrix for solving the circuit that finds the
    equivalent conductance of a circuit seen from Vin in QZPM. 
    The goal is to fill the part of the A matrix that depends on the 
    configuration of connections to Vin
    
    Kirchoff's law is expressed as AV = I in matrix form
    
    gm: is an array of sensor conductances
    gs_r: internal resistance of switches that select rows
    gs_c: internal resistance of switches that select columns
    row: row connected to Vin (if -1 no connection)
    col, col2: columns connected to Vin (if -1 no connection)
    opamp: indicates if an OA is connected to Vin with a conductance gd
    gd: conductance that connects the OA and the connecting node
    A: matrix to be filled
    """
    NR, NC = gm.shape
    if(opamp): f = 1.0
    else: f=0.0
    if(col == -1 and row>=0 and col2 == -1):
        A[row,-1] = -gs_r[row]
        A[-1, -1] = gs_r[row]+f*gd
        A[-1, row] = -gs_r[row]
    elif(row == -1 and col>=0 and col2 == -1):
        A[NR+col,-1] = -gs_c[col]
        A[-1,-1] = gs_c[col]+f*gd
        A[-1, NR+col] = -gs_c[col]
    elif(row >=0 and col>=0 and col2 ==-1):
        A[row, -1] = -gs_r[row]
        A[NR+col, -1] = -gs_c[col]
        A[-1,-1] = gs_r[row]+gs_c[col]+f*gd
        A[-1, row] = -gs_r[row]
        A[-1, NR+col] = -gs_c[col]
    elif(row == -1 and col>=0 and col2>=0 and col != col2):
        A[NR+col, -1] = -gs_c[col]
        A[NR+col2, -1] = -gs_c[col2]
        A[-1,-1] = gs_c[col]+gs_c[col2]+f*gd
        A[-1, NR+col] = -gs_c[col]
        A[-1, NR+col2] = -gs_c[col2]
    else:
        print('uuups row column selection not right')
        sys.exit(0)
    return
###
def geqQZPM(gm, gs_r, gs_c):
    """
    Obtains the equivalent conductance seen from the connecting node of a QZPM structure
    The results are obtained first row-colum selection (column changes first),
    then a single column (NC) then a single row (NR) connected to the connecting node
    Column column are not needed for the moment
    
    Inputs:
      gm: is an array of sensor conductances
      gs_r: internal resistance of switches that select rows
      gs_c: internal resistance of switches that select columns    
    Outputs:
      geq: a single vector of dimension NR*NC+NC+NR with all the results
    """
    NR, NC = gm.shape
    A = np.zeros((NR+NC+1, NR+NC+1))
    generateCommonMatrixG(gm, gs_r, gs_c, A)
    # The I vector is always the same
    I = np.zeros((NR+NC+1,1))
    I[-1,0] = 1.0 # Formally, we inject 1 A
    #
    geq = np.zeros(NR*NC+NC+NR) # number of experimental nodes measured
    count = 0
    for row in range(NR):
        for col in range(NC):
            # Set to zero before filling them with the right values
            A[-1,:] = 0.0 
            A[:,-1] = 0.0
            generateNodeEquations(gm, gs_r, gs_c, row, col, -1, opamp = False, gd = 0.0, A=A)
            v = sc.linalg.solve(A, I)
            #Iin = (vref-v[-1])*gd
            Iin = 1.0
            geq[count] = Iin/v[-1]
            count += 1
    # Then a column connected
    for col in range(NC):
        # Set to zero before filling them with the right values
        A[-1,:] = 0.0 
        A[:,-1] = 0.0
        generateNodeEquations(gm, gs_r, gs_c, row=-1, col=col, col2=-1, opamp = False, gd = 0.0, A=A)
        v = sc.linalg.solve(A, I)
        #Iin = (vref-v[-1])*gd
        Iin = 1.0
        geq[count] = Iin/v[-1]
        count += 1
    # Then, a row connected
    for row in range(NR):
        # Set to zero before filling them with the right values
        A[-1,:] = 0.0 
        A[:,-1] = 0.0
        generateNodeEquations(gm, gs_r, gs_c, row, col=-1, col2=-1,opamp = False, gd = 0.0, A=A)
        v = sc.linalg.solve(A, I)
        #Iin = (vref-v[-1])*gd
        Iin = 1.0
        geq[count] = Iin/v[-1]
        count += 1
    """
    # Finally, pairs of columns
    for col1 in range(NC):
        for col2 in range(col1+1, NC):
            # Set to zero before filling them with the right values
            A[-1,:] = 0.0 
            A[:,-1] = 0.0
            generateNodeEquations(gm, gs_r, gs_c, gd, row=-1, col=col1, col2=col2,opamp = False, gd = 0.0, A=A)
            v = sc.linalg.solve(A, I)
            # The measured voltage corresponds to the last of v after the OA
            vo[count] = vref-gd/gf*(v[-1]-vref)
            count += 1
    ##
    """
    return geq
###
def idealSolution(geqExp):
    """
    Given a set of equivalent conductances obtained after a scan of the matrix,
    NR*NC values, it finds the solution as if the switched were ideal
    
    Input:
      geqExp: set of NR*NC equivalent conductances
    Output:
      g0dum: a 1D vector of size NR*NC with sensor resistances
             column index changes first
      
    """
    # OJO A is not singular except if NR or NC are 2 (it seems after some tests)
    # It is not a real case
    # Column index change first to order variables
    NR, NC = geqExp.shape
    if(NR==2 or NC==2): 
        print('matrix too small')
        sys.exit(0)
    A = np.zeros((NR*NC, NR*NC))
    y = np.zeros((NR*NC,1))
    for row in range(NR):
        for col in range(NC):
            y[row*NC+col,0] = geqExp[row, col]
            # Construct matrix
            count = -1
            for ii in range(NR):
                for jj in range(NC):
                    count += 1
                    if(ii==row and jj!=col):
                        A[row*NC+col,count] = 1.0
                    if(jj==col and ii!=row):
                        A[row*NC+col,count] = 1.0
    g0dum = sc.linalg.solve(A, y)
    return g0dum
###
def voltageQZPM(gm, gs_r, gs_c, gd, gf, vref):
    """
    Obtains the output voltage of a QZPM structure with ideal OA
    
    Inputs:
      gm: is an array of sensor conductances
      gs_r: internal resistance of switches that select rows
      gs_c: internal resistance of switches that select columns
      gd: conductance that connects OA- to Vin
      gf: feedback conductance of OA
      vref: voltage level at V+    
    Outputs:
      vqzpm: Voltage for the corresponding scanning options
        It is a dictionary: 'vrc' a matrix when a row and a column are connected
        'vr': only a row connected, 'vc': only a column connected
        'vcc': pairs of columns connected. Given as a 1D vector in order 
        01, 02, ..., 12, 13, etc In the code, index 0 is a real column connected
    """
    NR, NC = gm.shape
    A = np.zeros((NR+NC+1, NR+NC+1))
    generateCommonMatrixG(gm, gs_r, gs_c, A)
    # The I vector is always the same
    I = np.zeros((NR+NC+1,1))
    I[-1,0] = vref*gd
    #
    vo = np.zeros(NR*NC+NR+NC+NC*(NC-1)//2) # number of experimental nodes measured
    count = 0
    for row in range(NR):
        for col in range(NC):
            # Set to zero before filling them with the right values
            A[-1,:] = 0.0 
            A[:,-1] = 0.0
            generateNodeEquations(gm, gs_r, gs_c, row, col, -1, opamp = True, gd = gd, A=A)
            v = sc.linalg.solve(A, I)
            # The measured voltage corresponds to the last of v after the OA
            vo[count] = vref-gd/gf*(v[-1]-vref)
            count += 1
    # Then a column connected
    for col in range(NC):
        # Set to zero before filling them with the right values
        A[-1,:] = 0.0 
        A[:,-1] = 0.0
        generateNodeEquations(gm, gs_r, gs_c, row=-1, col=col, col2=-1, opamp = True, gd = gd, A=A)
        v = sc.linalg.solve(A, I)
        # The measured voltage corresponds to the last of v after the OA
        vo[count] = vref-gd/gf*(v[-1]-vref)
        count += 1
    # Then, a row connected
    for row in range(NR):
        # Set to zero before filling them with the right values
        A[-1,:] = 0.0 
        A[:,-1] = 0.0
        generateNodeEquations(gm, gs_r, gs_c, row, col=-1, col2=-1,opamp = True, gd = gd, A=A)
        v = sc.linalg.solve(A, I)
        # The measured voltage corresponds to the last of v after the OA
        vo[count] = vref-gd/gf*(v[-1]-vref)
        count += 1
    # Finally, pairs of columns
    for col1 in range(NC):
        for col2 in range(col1+1, NC):
            # Set to zero before filling them with the right values
            A[-1,:] = 0.0 
            A[:,-1] = 0.0
            generateNodeEquations(gm, gs_r, gs_c, row=-1, col=col1, col2=col2,opamp = True, gd = gd, A=A)
            v = sc.linalg.solve(A, I)
            # The measured voltage corresponds to the last of v after the OA
            vo[count] = vref-gd/gf*(v[-1]-vref)
            count += 1
    ##
    # Return in a more convenient way
    vrc = vo[0:NR*NC].reshape(NR, NC)
    vc = vo[NR*NC:NR*NC+NC].copy()
    vr = vo[NR*NC+NC:NR*NC+NC+NR].copy()
    vcc = np.zeros((NC, NC))
    count = 0
    for col1 in range(NC):
        for col2 in range(col1+1, NC):
            vcc[col1, col2] = vo[NR*NC+NC+NR+count] 
            vcc[col2, col1] = vcc[col1, col2]
            count +=1
    ##
    vqzpm = {'vrc':vrc, 'vr': vr, 'vc': vc, 'vcc':vcc}
    return vqzpm
###
def hQZPM(row, col1, col2, vqzpm, rm, rd, rf, vref):
    """
    h(i,j) function of the QZPM paper
    Inputs: 
     row, col1, col2: indicate the row or columns connected to the connecting 
       node. -1 means no connection
     vqzpm: experimental values of voltages. Given as dictionary (see voltageQZPM):
     rm: switch resistance
     rd, rf, vref: parameters associated to the OA
    Output:
     h for the given inputs
    """
    # Use equation 4
    vorc = vqzpm['vrc']
    vor = vqzpm['vr']
    voc = vqzpm['vc']
    vocc = vqzpm['vcc']
    if(row >=0 and col1>=0 and col2<0):
        h = (vorc[row, col1]-vref)*rm/(vref*(rf+rd)-vorc[row, col1]*rd)
    elif(row >=0 and col1<0 and col2<0):
        h = (vor[row]-vref)*rm/(vref*(rf+rd)-vor[row]*rd)
    elif(row<0 and col1>=0 and col2<0):
        h = (voc[col1]-vref)*rm/(vref*(rf+rd)-voc[col1]*rd)
    elif(row<0 and col1>=0 and col2>=0):
        h = (vocc[col1, col2]-vref)*rm/(vref*(rf+rd)-vocc[col1, col2]*rd)
    else:
        print('uuups row col selection not possible in hQZPM')
        sys.exit(0)
    return h
###
def sensorQZPM(vqzpm, rm, rd, rf, vref):
    """
    Values of sensor conductances (matrix) using QZPM method
    Inputs:      
     vqzpm: experimental values of voltages. Given as dictionary (see voltageQZPM):
     rm: switch resistance
     rd, rf, vref: parameters associated to the OA.
    Output:
     Y: Admittance matrix associated to the sensor array
    """
    NR, NC = vqzpm['vrc'].shape
    Ac = np.zeros((NC, NC))
    for kk in range(NC):
        for jj in range(NC):
            if(kk!=jj):
                # equation 15
                Ac[jj, kk] = hQZPM(-1,  jj, -1, vqzpm, rm, rd, rf, vref)
                Ac[jj, kk] += hQZPM(-1, kk, -1, vqzpm, rm, rd, rf, vref)
                Ac[jj, kk] -= hQZPM(-1, kk, jj, vqzpm, rm, rd, rf, vref)
                Ac[jj, kk] *= 0.5
            else:
                # equation 6
                Ac[jj,jj] = 1.0-hQZPM(-1, jj, -1, vqzpm, rm, rd, rf, vref)
    Ar = np.zeros((NC, NR)) # Well it is slighty better to define in this order
    for ii in range(NR):
        for jj in range(NC):
            Ar[jj,ii] = hQZPM(ii, -1, -1, vqzpm, rm, rd, rf, vref)
            Ar[jj,ii] += hQZPM(-1, jj, -1, vqzpm, rm, rd, rf, vref)
            Ar[jj,ii] -= hQZPM(ii, jj, -1, vqzpm, rm, rd, rf, vref)
            Ar[jj,ii] *= 0.5
    #
    X = np.linalg.solve(Ac, Ar) # X is NCxNR
    X = X.T
    # Undo change of variable
    Y = np.zeros((NR, NC))
    Ym = 1.0/rm
    for ii in range(NR):
        AA = np.eye(NC)-np.tile(X[ii,:].reshape(NC,1), (1, NC))
        XY = (X[ii,:]*Ym).reshape(NC, 1)
        dum = np.linalg.solve(AA, XY)
        Y[ii,:] = dum[:,0]
    """
    # test
    for ii in range(NR):
        for jj in range(NC):
            print(X[ii,jj],Y[ii,jj]/(Ym+Y[ii,:].sum()))
    """
    return Y
###
def testgenerateNodeEquations():
    """
    Utility function to test the solution of the circuits with spice
    """
    def interpretSpiceOutput():
        # Utility function to extract values from spice output
        # Interpret output --- puf !!!
        f = open('kkkkqqqq.log', 'r')
        s = ''
        while('Node' not in s and 'Voltage' not in s):
            s = f.readline()
        # Two more lines
        s = f.readline()
        s = f.readline()
        res = {}
        # NR+NC+1 nodes. The extra node is connected to Iref
        for nn in range(NR+NC+1):
            s = f.readline()
            i1 = s.find(' ')
            key = s[1:i1]
            i2 = s.rfind(' ')
            val = float(s[i2+1:])
            res[key] = val
        # create vector for comparison
        vspice = np.zeros((NR+NC+1,1))
        for nn in range(NR):
            key = 'r'+str(nn)
            vspice[nn] = res[key]
        for nn in range(NC):
            key = 'c'+str(nn)
            vspice[nn+NR] = res[key]
        vspice[-1] = res['n0']
        f.close()
        return vspice
    ##
    np.random.seed(2021)
    for nsim in range(0,10):
        NR, NC = np.random.choice(range(2,7)), np.random.choice(range(2,7))
        gmin, gmax = 1/10e3, 1/100.0
        gm = gmin+(gmax-gmin)*np.random.rand(NR, NC)
        gs_r = gmin+(gmax-gmin)*np.random.rand(NR)
        gs_c = gmin+(gmax-gmin)*np.random.rand(NC)
        gd = gmin+(gmax-gmin)*np.random.rand()
        """
        NR, NC = 2,2
        rm = np.array([[100.0, 120],[140, 150]])
        gm = 1.0/rm
        gs_r = 1.0/np.array([50, 60.0])
        gs_c = 1.0/np.array([70, 80.0])
        gd = 1.0/170.0
        #vref = 1.0
        """
        A = np.zeros((NR+NC+1, NR+NC+1))
        generateCommonMatrixG(gm, gs_r, gs_c, A) # Fill common part
        I = np.zeros((NR+NC+1,1)) # I vector is always the same
        #I[-1,0] = vref*gd
        I[-1,0] = 1.0
        for row in range(-1,NR):
            for col in range(-1,NC):
                if(row == -1 and col == -1): continue
                A[-1,:] = 0.0
                A[:,-1] = 0.0
                generateNodeEquations(gm, gs_r, gs_c, row, col, col2 = -1, opamp = False, gd = 0, A=A)
                v = sc.linalg.solve(A, I)
                #print('----', row, col)
                #print(v.T)
                # Now do it using spice
                genSpice(row, col, csel2=-1, gm= gm, gs_r=gs_r, gs_c=gs_c)
                # Solve spice
                os.system('ngspice -o kkkkqqqq.log -b kkkkqqqq.cir > kkkkqqqq.txt')
                vspice = interpretSpiceOutput()
                if(not np.allclose(v, vspice, rtol = 5e-7)): # Spice gives only 6 decimal places. I don't know how to change
                    print('uuups spice differs')
                    import ipdb; ipdb.set_trace()
                    sys.exit(0)
        # Two columns
        row = -1
        for col in range(NC):
            for col2 in range(col+1,NC):
                if(row == -1 and col == -1): continue
                A[-1,:] = 0.0
                A[:,-1] = 0.0
                generateNodeEquations(gm, gs_r, gs_c, row=-1, col=col, col2 = col2, opamp = False, gd = 0, A=A)
                v = sc.linalg.solve(A, I)
                #print('----', row, col)
                #print(v.T)
                # Now do it using spice
                genSpice(rsel=-1, csel=col, csel2=col2, gm= gm, gs_r=gs_r, gs_c=gs_c)
                # Solve spice
                os.system('ngspice -o kkkkqqqq.log -b kkkkqqqq.cir > kkkkqqqq.txt')
                vspice = interpretSpiceOutput()
                if(not np.allclose(v, vspice, rtol = 5e-7)): # Spice gives only 6 decimal places. I don't know how to change
                    print('uuups spice differs cc')
                    print(v, vspice)
                    import ipdb; ipdb.set_trace()
                    sys.exit(0)

    return
###
def genSpice(rsel, csel, csel2, gm, gs_r, gs_c):
    """
    Utility function to test the solution with spice.
    """
    NR, NC = gm.shape
    f = open('kkkkqqqq.cir','w')
    f.write('Title \n')
    # Row - column connections
    for row in range(NR):
        if(row==rsel):
            line = 'RS'+str(row)+'_R'+' '+'R'+str(row)+' N0 '+str(1/gs_r[row])+'\n'
        else:
            line = 'RS'+str(row)+'_R'+' '+'R'+str(row)+' 0 '+str(1/gs_r[row])+'\n'
        f.write(line)
    for col in range(NC):
        if(col==csel or col==csel2):
            line = 'RS'+str(col)+'_C'+' '+'C'+str(col)+' N0 '+str(1/gs_c[col])+'\n'
        else:
            line = 'RS'+str(col)+'_C'+' '+'C'+str(col)+' 0 '+str(1/gs_c[col])+'\n'
        f.write(line)
    for row in range(NR):
        for col in range(NC):
            line = 'R'+str(row)+str(col)+' R'+str(row)+' C'+str(col)+' '+str(1/gm[row, col])+'\n'
            f.write(line)
    #f.write('RD N0 N1 '+str(1/gd)+'\n')
    #f.write('Vref N1 0 1.0\n')
    f.write('Iref 0 N0 1.0 \n')
    f.write('.OP\n.END')
    return
###
def lsqrSolution_QZPM(geqExp, NR, NC, g0 = None, xtol = 1.49012e-08, bounds = False, ftol = 1e-8, singleRm = False):
    """
    It finds the sensor resistances (and switch resistances in fact) in a QZPM configuration
    
    Inputs:
      geqExp: Set of equivalent conductances obtained from the experiments. 
        See geqQZPM for the order. It is a 1D vector
      NR, NC: number of rows and columns
      g0: initial solution, 1D vector. First NR*NC sensor valures, then column switch
        conductances, then row switch conductances (see singleRm too)
      xtol, ftol: values of tolerance for scipy routines
      bounds: whether to applu bound on the solution (conductances >=0)
      singleRm: whether to consider a single value for switch resistances.
        If singleRm is True, g0 has NR*NC+1 elements.
    """
    if(not singleRm):
        def fobj(x, geqExp, NR, NC): # objective function
            # It seems that x is passed as a 1D array, so take values from it
            # First array, then rows, then columns
            gm = x[0:NR*NC].reshape(NR, NC)
            gs_c = x[NR*NC:NR*NC+NC].copy()
            gs_r = x[NR*NC+NC:].copy()
            geq = geqQZPM(gm, gs_r, gs_c)
            return (geq-geqExp) # The square and sum is done by the optimization itself
    else:
        def fobj(x, geqExp, NR, NC): # objective function
            # It seems that x is passed as a 1D array, so take values from it
            # First array, then rows, then columns
            gm = x[0:NR*NC].reshape(NR, NC)
            gs_c = x[-1]*np.ones(NC)
            gs_r = x[-1]*np.ones(NR)
            geq = geqQZPM(gm, gs_r, gs_c)
            return (geq-geqExp) # The square and sum is done by the optimization itself
    ## 
    if(g0 is None):
        # Consider the solution with almost perfect switches
        # In the equations the order is col index changes first
        # OJO A is not singular except if NR or NC are 2 (it seems after some tests)
        # It is not a real case
        HIGH_G = 0.1 # 10 Ohms 
        g2d = geqExp[0:NR*NC].reshape(NR, NC)
        g0dum = idealSolution(g2d)
        if(not singleRm):
            g0 = np.zeros(NR*NC+NC+NR)
            g0[0:NR*NC] = g0dum[:,0] #+np.random.rand(NR*NC)*1e-6
            g0[NR*NC:] = HIGH_G+np.random.rand(NC+NR)*1e-5
        else:
            g0 = np.zeros(NR*NC+1)
            g0[0:NR*NC] = g0dum[:,0] #+np.random.rand(NR*NC)*1e-6
            g0[-1] = HIGH_G
    ##
    # g0 is already a 1D array
    if(not bounds):
        sol = scipy.optimize.leastsq(fobj, g0, (geqExp, NR, NC), xtol = xtol, ftol = ftol)
    else:
        sol = scipy.optimize.least_squares(fun = fobj, x0 =  g0, xtol = xtol, bounds = (0.0, np.inf), args = (geqExp, NR, NC), gtol = 1e-10)
    # First element of the solution is the conductance matrix itself
    if(not bounds):
        gdum = sol[0]
    else:
        gdum = sol['x']
    gm = gdum[0:NR*NC].reshape(NR, NC)
    if(not singleRm):
            gs_c = gdum[NR*NC: NR*NC+NC]
            gs_r = gdum[NR*NC+NC:]
    else:
        gs_c = gdum[-1]*np.ones(NC)
        gs_r = gdum[-1]*np.ones(NR)
    return gm, gs_c, gs_r, sol
###
def testSensorQZPM():
    """
    Utility function to find QZPM solution and compare with real values
    """
    np.random.seed(2021)
    for nn in range(10):
        NR, NC = np.random.choice(range(2,7)), np.random.choice(range(2,7))
        gmin, gmax = 1/10e3, 1/100.0
        gm = gmin+(gmax-gmin)*np.random.rand(NR, NC)
        rs = np.random.rand()*10+10.0
        gs = 1.0/rs
        rd = 90.0+np.random.rand()*10.0
        gd = 1.0/rd
        rf = 140.0+np.random.rand()*10.0
        gf = 1.0/rf
        vref = 1.0+np.random.rand()
        # Voltage of a QZPM with ideal OA. Same value of switch resistance
        vqzpm = voltageQZPM(gm, gs*np.ones(NR), gs*np.ones(NC), gd, gf, vref)
        # Recover using QZPM 
        gm2 = sensorQZPM(vqzpm, rs, rd, rf, vref) # Deduced sensor values using QZPM
        if(not np.allclose(gm2, gm)):
            print('uuuups simulation nn does not give the same results')
    return
###
def testLsqr_QZPM():
    """
    Utility function to the the adaptation of IECM to QZPM
    """
    import time
    np.random.seed(2021)
    print('OJO the initial value is very important')
    ################## DO NOT CHANGE THIS EXAMPLE
    for singleRm in [False, True]:
        t1 = time.time()
        #NR, NC = 3, 4 # This works well with HIGH_G = 0.1, the same for all and may be a little tolerance
        NR, NC = 4, 4 
        gmin, gmax = 1/10e3, 1/100.0
        gm = gmin+(gmax-gmin)*np.random.rand(NR, NC)
        gs_min, gs_max = 1/20.0, 1/10.0
        if(not singleRm):
            gs_r = gs_min+(gs_max-gs_min)*np.random.rand(NR)
            gs_c = gs_min+(gs_max-gs_min)*np.random.rand(NC)
        else:
            gs = gs_min+(gs_max-gs_min)*np.random.rand()
            gs_r = gs*np.ones(NR)
            gs_c = gs*np.ones(NC)
        geqExp = geqQZPM(gm, gs_r, gs_c)
        #import ipdb;ipdb.set_trace()
        gm1, gs_c1, gs_r1, sol = lsqrSolution_QZPM(geqExp, NR, NC, bounds=True, singleRm = singleRm)
        print(abs(gm-gm1).sum())
        print(abs(gs_r-gs_r1).sum())
        print(abs(gs_c-gs_c1).sum())
        print(sol['fun'])
        e1 = abs(gm1-gm)/gm
        print('error rel ', e1.mean(), e1.max())
        print(time.time()-t1)
        ####
###
