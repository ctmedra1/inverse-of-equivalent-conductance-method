Code associated to the inverse of equivalent conductance method (IECM) to remove crosstalk in resistive sensor arrays. The first paper is:

C. Medrano et al., "Circuit Analysis of Matrix-Like Resistor Networks for Eliminating Crosstalk in Pressure Sensitive Mats", doi: 10.1109/jsen.2019.2918592, IEEE Sensors Journal 19.18 (2019): 8027-8036

Then several other papers might have been published. A first version of the basic functions in this project was published in oceanCode: doi: https://doi.org/10.24433/CO.7641012.v1

To understand crosstalk see also J. Wu, "Scanning Approaches of 2-D Resistive Sensor Arrays: A Review", doi: 10.1109/JSEN.2016.2641001, IEEE Sensors Journal  17.4 (2017): 914 - 925

The python directory contains the code in Python. The main routines are there, together with examples (see README in each directory). You should look at crosstalk.py to understand the basic code.

Then, the directory articles contains code related to articles. See each one and its README file
